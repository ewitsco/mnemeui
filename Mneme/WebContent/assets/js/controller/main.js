var mainApp = angular.module("mainApp", ['ngRoute','ngStorage']);
/*
angular.element(document).ready(function ($scope,$http,$sessionStorage,$location,$window) {
	if($sessionStorage.loggedin==undefined)
	{
		$window.location.href=("./login.html");
	}else{
		if($sessionStorage.loggedin==true){
			$window.location.href=("./#event");
		}else{
		$window.location.href=("./login.html");
		}
	}
});*/

		
		
mainApp.filter('range', function() {
	  return function(input, min, max) {
	    min = parseInt(min);
	    max = parseInt(max);
	    for (var i=min; i<=max; i++)
	      input.push(i);
	    return input;
	  };
});
	  
mainApp.directive('aDatepicker', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            el.datepicker({
            	dateFormat:'yy-mm-dd', minDate: 0
            });
            var component = el.siblings('[data-toggle="datepicker0"]');
        
            if (component.length) {
                component.on('click', function () {
                    el.trigger('focus');
                });
            }
            
        }
    };
});

mainApp.directive('bDatepicker', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
        	el.datepicker({
            	dateFormat:'yy-mm-dd'
            });
            var component = el.siblings('[data-toggle="datepicker1"]');
            if (component.length) {
                component.on('click', function () {
                    el.trigger('focus');
                });
            }
            
        }
    };
});
mainApp.directive('cDatepicker', function () {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
        	el.datepicker({
            	dateFormat:'yy-mm-dd'
            });
            var component = el.siblings('[data-toggle="datepicker2"]');
            if (component.length) {
                component.on('click', function () {
                    el.trigger('focus');
                });
            }
            
        }
    };
});


mainApp.config(function($routeProvider) {
	$routeProvider
		.when('/home', {
			templateUrl: 'home.html',
			controller: 'homeController'
		})
		.when('/user', {
			templateUrl: 'profile.html',
			controller: 'userController'
		})
		
		.when('/event', {
			templateUrl: 'event.html',
			controller: 'eventlistController'
		})
		.when('/reminder', {
			templateUrl: 'reminder.html',
			controller: 'PrefController'
		})
		
		.when('/channels', {
			templateUrl: 'channels.html',
			controller: 'commController'
		})
		
		.when('/welcome', {
			templateUrl: 'welcome.html',
			controller: 'welcomeController'
		})
				
		.when('/reset', {
			templateUrl: 'Reset.html',
			controller: 'resetController'
		})
				
		.when('/otherwise', {
			templateUrl: 'temp.html',
			controller: 'tempController'
		})
		.otherwise({
		redirectTo: '/otherwise'
		})
		
});

mainApp.controller('tempController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){
	
	
	
	if($sessionStorage.loggedin==true)
	{
	console.log("here1");
console.log($sessionStorage.cronofy);
	if($sessionStorage.cronofy){
		$scope.url =$window.location.href;
	//	$sessionStorage.cronofy=false;
		var finalurl=$scope.url;
		console.log('finalurl'+finalurl);
		if(finalurl.indexOf('code') !== -1)
			{
			$sessionStorage.loggedin=true;
			var indexofcode=finalurl.indexOf('=');
			var indexofand=finalurl.indexOf('&');
			$scope.code = finalurl.substring(indexofcode+1,indexofand);
			var code=$scope.code;
			var userid=$sessionStorage.user.userId;
		console.log('code'+code);
			
			$http({
			    method : 'GET',
			    url : './rest/auth?userid='+userid+'&code='+code,
			    headers: { 'Content-Type':'application/json;charset=UTF-8'}
			}).success(function (data, status, headers, config) {
				
				    console.log(status);
				    $scope.success=' Authorization process successfull';
				    $window.location.href=(".#/user");
				    //$window.location.href=("http://localhost:8080/Mneme/#/home");
			    
			})
			.error(function (data, status, headers, config) {
			   			 console.log(status);
			   			
			   		  if(data ==undefined){
						   $scope.error = "System Error. Please try again";
				   }else{ if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
				    	 $scope.error = "System Error. Please try again";
				     }
				     else{
				    	 $scope.error = data.errorMessage;
				     }}

			   					});

			
			 }
		
		else
			{
			
			 $window.location.href=(".#/user");
				
			}
	}
	else{
		$window.location.href=("./login.html");
	}
	}
	else{
		$window.location.href=("./login.html");
	}
	console.log("here2");
	
	console.log("here3");
}]);




mainApp.controller('welcomeController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){
	
	$scope.username=$sessionStorage.user.name;
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	
}]);




mainApp.controller('resetController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){
	
	$scope.username=$sessionStorage.user.name;
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	
$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
			   

			 
		});
	
		};
	
	
	$scope.cancelForm=function()
	{
		  $location.path("/user");
		
	}
	
	
	$scope.submitForm = function(){
		
		if($scope.user==undefined){
			$scope.error='Please enter required information ';
			return;
		}
		
		if($scope.user.password==undefined)
		{
		$scope.error="please enter current password";
		return;
		}
	if($scope.user.newpassword==undefined)
		{
		$scope.error="please enter new password";
		return;
		}
	if($scope.user.password_confirm==undefined)
		{
		$scope.error="please enter confirm password";
		return;
		}
   if($scope.user.newpassword!=$scope.user.password_confirm)
	   {
	   $scope.error="New password and confirm password are not matching";
		return;
	   }
		
		
		
		
		console.log($scope.user);
		var logindata={};
		logindata.login= $sessionStorage.user.login;
		logindata.password=$scope.user.password;
		
		$http({  
            method : 'POST',  
            url : "./rest/loginservice",  
            data : angular.toJson(logindata),  
            
		}).success(function (data, status, headers, config) { 
			console.log(angular.toJson(data));
			$scope.usergot=angular.fromJson(data);
			console.log($scope.usergot);
			$scope.usergot.password=$scope.user.newpassword;
			
			$http({  
	            method : 'POST',  
	            url : "./rest/pwdReset",  
	            data : angular.toJson($scope.usergot),  
	            
			}).success(function (data, status, headers, config) { 
				$scope.error = false;
				$scope.success="Password reset Successfull";
				$scope.user.password="";
				$scope.user.newpassword="";
				$scope.user.password_confirm="";
			})
			
			 .error(function (data, status, headers, config) { 
				 
        	
				  if(data ==undefined){
					   $scope.error = "System Error. Please try again";
			   }else{ 
				   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
			    	 $scope.error = "System Error. Please try again";
			     }
			     else{
			    	 $scope.error = data.errorMessage;
			     }
				   }	   
           $scope.user.password="";
			$scope.user.newpassword="";
			$scope.user.password_confirm="";
           
           
        });
			
			/*alert("after submit");
            alert(status);*/
        })
        .error(function (data, status, headers, config) { 
        	console.log(angular.toJson(data));
        	console.log(status);
        	if(status==403)
     	   {
     	  $sessionStorage.loggedin=false;
     	   $scope.error="Please Login to continue";
     	   $location.path("/home");

     	   }else{
			  
        		
     		  if(data ==undefined){
				   $scope.error = "System Error. Please try again";
		   }else{ 
			   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.error = data.errorMessage;
		     }
			   }

			  
				 
     	   }
           var response=data;
           $scope.user.password="";
			$scope.user.newpassword="";
			$scope.user.password_confirm="";
           console.log(angular.toJson(response));
        });


	
	};
	
	
	
	
	
}]);










mainApp.controller('homeController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){	
	$scope.url =$window.location.href;
//	$sessionStorage.cronofy=false;
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	var finalurl=$scope.url;
	console.log('finalurl'+finalurl);
	if(finalurl.indexOf('code') !== -1)
		{
		$sessionStorage.loggedin=true;
		var indexofcode=finalurl.indexOf('=');
		var indexofand=finalurl.indexOf('&');
		$scope.code = finalurl.substring(indexofcode+1,indexofand);
		var code=$scope.code;
		var userid=$sessionStorage.user.userId;
	console.log('code'+code);
		
		$http({
		    method : 'GET',
		    url : './rest/auth?userid='+userid+'&code='+code,
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {
			 var response=data;
			    console.log(response);
			    $scope.success=' Authorization process successfull';
			    $window.location.href=(".#/user");
			    //$window.location.href=("http://localhost:8080/Mneme/#/home");
		    
		})
		.error(function (data, status, headers, config) {
		   			 console.log(status);
		   			
		   		  if(data ==undefined){
					   $scope.error = "System Error. Please try again";
			   }else{ 
				   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
			    	 $scope.error = "System Error. Please try again";
			     }
			     else{
			    	 $scope.error = data.errorMessage;
			     }
				   }

		   					});

		
		 }
	
	else
		{
		
		 $window.location.href=(".#/user");
			
		}
	// 
	
	
	
	
	$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		   /* $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;*/
		  
	    	$location.path("./login.html");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		   /* $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;*/
		 
		   
	    	 $location.path("./login.html");
			   

			 
		});
		$window.location.href=(".#/login.html");
		};
	
	$scope.login = function(){
		/*alert("in function");
		alert(angular.toJson($scope.user));*/
		//authorization();
		$window.location.href=(".#/login.html");

	};
	

	$scope.gotologin=function(){
		$location.path("./login.html");
	};
}]);




mainApp.controller('PrefController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){
	
	$sessionStorage.cronofy=false;
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
			   

			 
		});
	
		};
	
	
	
	$sessionStorage.eventNavigate=false;
	$sessionStorage.fromdate_cache=undefined;
	$sessionStorage.todate_cache=undefined;
	$scope.username=$sessionStorage.user.name;
	//console.log($sessionStorage.user);
//	var userid=$sessionStorage.user.userId;
	var userid=$sessionStorage.user.userId;
console.log(userid);
function getpref(){
	$scope.Prefelement={};

	$http({
	    method : 'GET',
	    url : "./rest/getreminder/"+userid      

	}).success(function (data, status, headers, config) {
		console.log(status);
		
		console.log('iam here start');
	console.log(angular.toJson(data));
	$scope.Prefelement=data;
	console.log(angular.toJson($scope.Prefelement));

		/*alert("after submit");
	    alert(status);*/
	})
	.error(function (data, status, headers, config) {
		console.log(status);
		if(status==403)
		   {
		  $sessionStorage.loggedin=false;
		   $scope.error="Please Login to continue";
		   $location.path("/home");

		   }else{
			  
			   if(data ==undefined){
				   $scope.error = "System Error. Please try again";
		   }else{ 
			   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.error = data.errorMessage;
		     }
			   }
		   }
	  
	});
	};
	
function getchannel(){
	$scope.channel_list=[]; 
				
		$http({
			        method : 'GET',
			        url : "./rest/getchannels/"+userid       
				}).success(function (data, status, headers, config) {
					console.log(status);
						
					channel_length=data.channels.length;	
			var response_data=data;
			var data = response_data;
							
			for(var i = 0, l = channel_length; i < l; i++) {			
				$scope.channel_list.push( data.channels[i].type  );	
				
				}	
			$scope.showpref=false;
			$scope.showpref_link=false;
			
			
			
			
			getpref();
			if($scope.channel_list.length==0){
			//	 $scope.Prefelement.reminderPreferences = [ {channelType : '', reminderNumber : '',reminderType : ''} ];
				$scope.showpref=true;
				$scope.showpref_link=false;
				
			}
			if($scope.channel_list.length>0 ){
				
				// $scope.Prefelement.reminderPreferences = [ {channelType : '', reminderNumber : '',reminderType : ''} ];
				$scope.showpref1=false;
				$scope.showpref_link=true;
			}
			    })
			    .error(function (data, status, headers, config) {
			    	console.log(status);
			    	
			    	  if(data ==undefined){
						   $scope.error = "System Error. Please try again";
				   }else{ 
					   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
				    	 $scope.error = "System Error. Please try again";
				     }
				     else{
				    	 $scope.error = data.errorMessage;
				     }
					   }

			    });
	
};

getchannel();

/*console.log('inside pref'+$sessionStorage.channel_list);
if($sessionStorage.channel_list.length==0){
	
	$scope.showpref=true;
	$scope.showpref_link=false;
	
}
if($sessionStorage.channel_list.length>0 ){
	
	 $scope.Prefelement.reminderPreferences = [ {channelType : '', reminderNumber : '',reminderType : ''} ];
	$scope.showpref1=false;
	$scope.showpref_link=true;
}
console.log('vl'+angular.toJson($scope.Prefelement.reminderPreferences));*/
	$scope.channels = [];
	$scope.channels=$scope.channel_list;
	// selectedChoice: ko.observable($scope.Prefelement.reminderPreferences.channelType) ;
	   

	$scope.units = ['WEEKS', 'DAYS', 'HOURS','MINUTES'];

	console.log($sessionStorage.user);
	var counter=0;
	// $scope.Prefelement.reminderPreferences.push(  {  channelType : '', reminderNumber : '',reminderType : ''} );

    $scope.newItem = function($event){
        counter++;
        $scope.Prefelement.reminderPreferences.push(  {  channelType : '', reminderNumber : '',reminderType : ''} );
        $event.preventDefault();
    };
    $scope.inlinef= function($event,inlinecontrol){
        var checkbox = $event.target;
        if(checkbox.checked){
            $('#'+ inlinecontrol).css('display','inline');
        }else{
            $('#'+ inlinecontrol).css('display','');
        }

    }
    $scope.showitems = function($event){
        $('#displayitems').css('visibility','none');
    }

	$scope.user=$sessionStorage.user;
	console.log($scope.user);
	

	$scope.submitForm = function(){
		$scope.error=undefined;
		$scope.success=undefined;
		var correct1=true;
		var correct2=true;
		var correct3=true;
		var data=$scope.Prefelement.reminderPreferences;
				
		   angular.forEach($scope.Prefelement.reminderPreferences,function(value,index){
        
			  
			   
			   if(value.channelType=="")
	        	{
				   
	        	     correct1=false;
	        	
	        	}
			   
			   if(value.reminderNumber=="")
	        	{
	        	     correct2=false;
	        	
	        	}
			   
			   			   
			   if(value.reminderType=="")
        	     {
        	     correct3=false;
        	     
        	    }
           });
		
		
		 if(correct1==false)
			 {
			
			 $scope.error='Please provide channel ';
			 return;
			 }

		 
			 if(correct2==false)
				 {
				 $scope.error='Please provide Reminder Number ';
				 return;
				 }


			 if(correct3==false)
				 {
				 $scope.error='Please provide Reminder type ';
				 return;
				 }

		   
		   
		var channel_req= {
			        "userId": userid,
			        "reminderPreferences": $scope.Prefelement.reminderPreferences,
			      
		};
		/*alert("in function");*/
		console.log(angular.toJson(channel_req));
		$http({
            method : 'POST',
            url : "./rest/updaterempref",
            data : channel_req,

		}).success(function (data, status, headers, config) {
		//	console.log(angular.toJson(data));
			
			console.log(status);
			

			console.log($sessionStorage.user);
$scope.success="Preference Saved Successfully";
			/*alert("after submit");
            alert(status);*/
        })
        .error(function (data, status, headers, config) {
        	
        	console.log(status);
        	if(status==403)
     	   {
     	  $sessionStorage.loggedin=false;
     	   $scope.error="Please Login to continue";
     	   $location.path("/otherwise");

     	   }else{
     		  
     		  if(data ==undefined){
				   $scope.error = "System Error. Please try again";
		   }else{ 
			   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.error = data.errorMessage;
		     }
			   }
     	   }
           var response=data;

           console.log(angular.toJson(response));
        });



	};

	$scope.submitCancel= function(){
		  $location.path("/event");
		
	};
	}]);


mainApp.controller('commController', ['$scope','$http','$sessionStorage','$location','$window', function($scope,$http,$sessionStorage,$location,$window){
	$sessionStorage.eventNavigate=false;
	$sessionStorage.fromdate_cache=undefined;
	$sessionStorage.todate_cache=undefined;
	$scope.edit1=false;
	$scope.edit2=false;
	$scope.edit3=false;
	$scope.edit4=false;
	
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	
	
	$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
			   

			 
		});
	
		};
		$scope.login = function(){
			/*alert("in function");*/
			$window.location.href=(".#/otherwise");
			};
	
		
		
		var userid=$sessionStorage.user.userId;
		$scope.username=$sessionStorage.user.name;
	console.log(userid);


		
		var response_data=null;
		var channel_length=null;
		


	$sessionStorage.channel_list=[];




		/*alert("before submit");*/
	$scope.IsVisible1 = false;
	$scope.IsVisible2 = false;
	$scope.IsVisible3 = false;
	$scope.IsVisible4 = false;
	$scope.ShowHide1 = function () {
	    //If DIV is visible it will be hidden and vice versa.
	    $scope.IsVisible1 = $scope.IsVisible1 ? false : true;
	};
	$scope.ShowHide2 = function () {
	    //If DIV is visible it will be hidden and vice versa.
	    $scope.IsVisible2 = $scope.IsVisible2 ? false : true;
	};
	$scope.ShowHide3 = function () {
	    //If DIV is visible it will be hidden and vice versa.
	    $scope.IsVisible3 = $scope.IsVisible3 ? false : true;
	};
	$scope.ShowHide4 = function () {
	    //If DIV is visible it will be hidden and vice versa.
	    $scope.IsVisible4 = $scope.IsVisible4 ? false : true;
	};

	   
	  
	    
	   
	    $http({
	        method : 'GET',
	        url : "./rest/getchannels/"+userid       

		}).success(function (data, status, headers, config) {
			console.log(status);
			
			console.log('iam here start');
			console.log('lenght is'+data.channels.length);
			channel_length=data.channels.length;
	console.log(data);
	var response_data=data;
			/*alert("after submit");
	        alert(status);*/
	var data = response_data;
	$scope.communication={}; 	
	for(var i = 0, l = channel_length; i < l; i++) {
		
		$sessionStorage.channel_list.push( data.channels[i].type  );
		
		console.log(data.channels[i].type);
		if (data.channels[i].type=="EMAIL"){
			$scope.IsVisible1 = true;
			 $scope.ShowHide1 = false;
			 $scope.isSelected1 = !$scope.isSelected1;
			console.log($scope.IsVisible1);
			var test=data.channels[i].value;
			console.log($scope.communication.email);
			console.log(test);
			$scope.communication.email=data.channels[i].value;
		}
		if (data.channels[i].type=="SMS"){
			$scope.IsVisible2 = true;	
			 $scope.ShowHide2 = false;
			 $scope.isSelected2 = !$scope.isSelected2;

			$scope.communication.phone=data.channels[i].value;
			console.log('Iam here2');
		}
		if (data.channels[i].type=="FACEBOOK"){
			$scope.IsVisible3 = true;
			 $scope.ShowHide3 = false;
			$scope.isSelected3 = !$scope.isSelected3;
			$scope.communication.facebook=data.channels[i].value;
		}
		if (data.channels[i].type=="WHATSAPP"){
			$scope.IsVisible4 = true;
			 $scope.ShowHide4 = false;
			$scope.isSelected4 = !$scope.isSelected4;
			$scope.communication.whatsapp=data.channels[i].value;
		}
		}
		
	console.log('chanellist'+$sessionStorage.channel_list);
	    
	    })
	    .error(function (data, status, headers, config) {
	    	console.log(status);
	    	if(status==403)
	 	   {
	 	  $sessionStorage.loggedin=false;
	 	   $scope.error="Please Login to continue";
	 	   $location.path("/otherwise");

	 	   }else{
	 		  
	 		  if(data ==undefined){
				   $scope.error = "System Error. Please try again";
		   }else{ 
			   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.error = data.errorMessage;
		     }
			   }
	 	   }
	      
	    });
	    
	    
	    $scope.deleteemail=function()
	    {
	    	$scope.communication.email=undefined;
	    	$scope.isSelected1=false;
	    	$scope.IsVisible1=false;
	    	
	    };
	    
	    $scope.deletephone=function()
	    {
	    	$scope.communication.phone=undefined;
	    	$scope.isSelected2=false;
	       	$scope.IsVisible2=false;
	    };
	    $scope.deletefb=function()
	    {
	    	$scope.communication.facebook=undefined;
	    	$scope.isSelected3=false;
	    	$scope.IsVisible3=false;
	    };
	    $scope.deletewapp=function()
	    {
	    	$scope.communication.whatsapp=undefined;
	    	$scope.isSelected4=false;
	    	$scope.IsVisible4=false;
	    };
	    
	    
	    
	   
	    
		$scope.submitForm = function(){
			
			$scope.error=undefined;
			$scope.success=undefined;
			console.log('subit after'+$scope.isSelected1);
			 $scope.channels = [] ;
	if ($scope.isSelected1){
		
		console.log('$scope.communication.email'+$scope.communication.email);
		if($scope.communication.email==undefined){
			$scope.error='Please enter the email id';
			return;
		}
		console.log('$scope.communication.email'+$scope.communication.email);
		
		if($scope.communication.email!=null && !!($scope.communication.email)){
			console.log('$scope.communication.email inside'+$scope.communication.email);
			var x = $scope.communication.email;
		    var atpos = x.indexOf("@");
		    var dotpos = x.lastIndexOf(".");
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
				$scope.error='please enter valid email ';
		        return;
		    }
		$scope.channels.push(   {type : 'EMAIL',value : $scope.communication.email} );
		}
	}
	
	
	
	if ($scope.isSelected2){
		if($scope.communication.phone==undefined){
			$scope.error='Please enter the SMS phone number';
			return;
		}
		if(!($scope.communication.phone=='' || $scope.communication.phone==null || $scope.communication.phone==undefined)){
			  if (!/^[0-9]{10}$/.test($scope.communication.phone)) {
				  $scope.error='Phone is not valid , please enter 10 digit number ';
					return;
			  }
		}
		console.log('$scope.communication.phone'+$scope.communication.phone);
		$scope.channels.push(   {type : 'SMS',value : $scope.communication.phone} );
	}

/* duplicate from above.
	if ($scope.isSelected2){
		if($scope.communication.phone==undefined){
			$scope.error='Please enter the SMS phone number';
			return;
		}
		if(!($scope.communication.phone=='' || $scope.communication.phone==null || $scope.communication.phone==undefined)){
			  if (!/^[0-9]{10}$/.test($scope.communication.phone)) {
				  $scope.error='Phone is not valid , please enter 10 digit number ';
					return;
			  }
		}
		console.log('$scope.communication.phone'+$scope.communication.phone);
		$scope.channels.push(   {type : 'SMS',value : $scope.communication.phone} );
	}
	*/
	
	if ($scope.isSelected3){
		if($scope.communication.facebook==undefined){
			$scope.error='Please enter the facebook id';
			return;
		}
	
		if($scope.communication.facebook!=null && !!($scope.communication.facebook)){
		
			var x = $scope.communication.facebook;
		    var atpos = x.indexOf("@");
		    var dotpos = x.lastIndexOf(".");
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
				$scope.error='please enter valid facebook id ';
		        return;
		    }
		    $scope.channels.push(   {type : 'FACEBOOK',value : $scope.communication.facebook} );
		    
		}
	}
	if ($scope.isSelected4){
		if($scope.communication.whatsapp==undefined){
			$scope.error='Please enter the whatsapp number';
			return;
		}
		if($scope.communication.whatsapp!=null && !!($scope.communication.whatsapp)){
		$scope.channels.push(   {type : 'WHATSAPP',value : $scope.communication.whatsapp} );
		}
	}
			   
	console.log($scope.channels);		        
			       
			    
	console.log(angular.toJson($scope.channels));

	var channel_req= {
		        "userId": userid,
		        "channels": $scope.channels,
		      
	};
	console.log('reqpare'+angular.toJson(channel_req));
			
			
			
			/*alert("in function");
			alert(angular.toJson($scope.user));*/
			$http({
	            method : 'POST',
	            url : "./rest/commChannel",
	            data : channel_req,

			}).success(function (data, status, headers, config) {
				console.log(status);
				$scope.error=false;
				$scope.success='Saved successfully.. ';
					//$location.path("/user");


				/*alert("after submit");
	            alert(status);*/
	        })
	        .error(function (data, status, headers, config) {
	        	console.log(status);
	        	if(status==403)
	     	   {
	     	  $sessionStorage.loggedin=false;
	     	 $scope.success=false;
	     	   $scope.error="Please Login to continue";
	     	   $location.path("/otherwise");

	     	   }else{
	     		 
	     		  if(data ==undefined){
					   $scope.error = "System Error. Please try again";
			   }else{ 
				   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
			    	 $scope.error = "System Error. Please try again";
			     }
			     else{
			    	 $scope.error = data.errorMessage;
			     }
				   }

	     	   }
	          
	        });



		};
		$scope.submitsetpreference=function(){
			$scope.channels = [] ;
			if ($scope.isSelected1){
				
				if($scope.communication.email==undefined){
					$scope.error='Please enter the email id';
					return;
				}
				
				if($scope.communication.email!=null && !!($scope.communication.email)){
					
					var x = $scope.communication.email;
				    var atpos = x.indexOf("@");
				    var dotpos = x.lastIndexOf(".");
				    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
						$scope.error='please enter valid email ';
				        return;
				    }
				$scope.channels.push(   {type : 'EMAIL',value : $scope.communication.email} );
				}
				
			}
			if ($scope.isSelected2){
				if($scope.communication.phone==undefined){
					$scope.error='Please enter the phone number';
					return;
				}
				if($scope.communication.phone!=null && !!($scope.communication.phone)){
				$scope.channels.push(   {type : 'SMS',value : $scope.communication.phone} );
				}
			}
			if ($scope.isSelected3){
			
				if($scope.communication.facebook==undefined){
					$scope.error='Please enter the facebook id';
					return;
				}
			
				if($scope.communication.facebook!=null && !!($scope.communication.facebook)){
					
					var x = $scope.communication.facebook;
				    var atpos = x.indexOf("@");
				    var dotpos = x.lastIndexOf(".");
				    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
						$scope.error='please enter valid facebook id ';
				        return;
				    }
				}
			}
			if ($scope.isSelected4){
				if($scope.communication.whatsapp==undefined){
					$scope.error='Please enter the whatsapp number';
					return;
				}
				if($scope.communication.whatsapp!=null && !!($scope.communication.whatsapp)){
				$scope.channels.push(   {type : 'WHATSAPP',value : $scope.communication.whatsapp} );
				}
			}
			var channel_req= {
				        "userId": userid,
				        "channels": $scope.channels,
				      
			};
			console.log('reqpare'+angular.toJson(channel_req));
					
					
					
					/*alert("in function");
					alert(angular.toJson($scope.user));*/
					$http({
			            method : 'POST',
			            url : "./rest/commChannel",
			            data : channel_req,
					}).success(function (data, status, headers, config) {
						console.log(status);
						
						$scope.error='Saved successfully.. ';
						//	$location.path("/user");
						/*alert("after submit");
			            alert(status);*/
			        })
			        .error(function (data, status, headers, config) {
			        	console.log(status);
			        	
					     if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
					    	 $scope.error = "System Error. Please try again";
					    	 return;
					     }
					     else{
					    	 $scope.error = data.errorMessage;
					    	 return;
					     }

			          
			        });
			$location.path("/pref");
		};
	$scope.submitCancel=function(){
		$location.path("/event");
		};
		}]);




mainApp.controller('eventlistController', ['$scope','$http','$sessionStorage','$location', '$window',function($scope,$http,$sessionStorage,$location,$window){
	
	
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
			   

			 
		});
	
		};
	
	
	
	
	var userid=$sessionStorage.user.userId;
	$scope.username=$sessionStorage.user.name;
console.log(userid);
	
	var counter1=0;
	$scope.newItemevent = function($event){
	    counter1++;
	    if($scope.questionelemnt==undefined){
	    	$scope.questionelemnt = [ {email : '',name : '',phone : ''} ];
	    }
	    else{
	    $scope.questionelemnt.push(  {email : '',name : '',phone : ''} );
	    }
	    $event.preventDefault();
	};	
	
	

	$scope.callcollapsed=function(){
		console.log('inside create event');
		console.log('$scope.collapsed'+$scope.collapsed);
		$scope.collapsed=true;
		$scope.Event=undefined;
		$scope.questionelemnt=undefined;
		$scope.cmin=undefined;
$scope.chour=undefined;
$scope.campm=undefined;
	};
	
	
	
	
	$scope.refresh=function()
	{
		 var currentDate = new Date();
    	 var day1 = currentDate.getDate();
    	 var month1 = currentDate.getMonth() + 1;
    	var day=pad(day1);
    	var month=pad(month1);
    	 var year = currentDate.getFullYear();
    	 var today = year+'/'+month+'/'+day;
    	 console.log('today'+today);
    	 console.log("i am here");
    	 loaddata(today,today);
    	
	}
	
	
  $scope.createevent=function(){	
	$scope.c_error=undefined;
	$scope.c_success=undefined;
	  var noerror=false;
	if (!(angular.isDefined($scope.Event)) ){
	    
	    $scope.c_error = "Please fill Event Name, date and time";
	        	    noerror=true;
	        	
	        	    return;
	  }
	
	
	if (!(angular.isDefined($scope.Event.eventName))) {
		$scope.c_error = "Please fill Event Name, date and time";
		noerror=true;
		
		 return;
	  }
	if (!(angular.isDefined($scope.cmin))) {
		$scope.c_error = "Please fill Event Name, date and time";
		noerror=true;
		
		 return;
		
	  }
	if (!(angular.isDefined($scope.chour))) {
		$scope.c_error = "Please fill Event Name, date and time";
		noerror=true;
		
		 return;
	  }
	if (!(angular.isDefined($scope.Event.eventDate))) {
		$scope.c_error = "Please fill Event Name, date and time";
		noerror=true;
	
		 return;	
	  }
	if(!noerror){
	console.log(noerror);
	console.log($scope.Event.eventDate);
	console.log($scope.Event.eventName);
	console.log($scope.cmin.min);
	console.log($scope.chour.hour);
	if(angular.equals({}, $scope.Event)){
		console.log("eventname is blank");
	}else{
	console.log($scope.Event);
	var hr= parseInt($scope.chour.hour, 10); 
	var myhr=$scope.chour.hour;
	console.log($scope.campm);
	if($scope.campm=='pm' && myhr!=12){
		 
		
		console.log(hr);
		console.log(hr+12);
		hr=hr+12;
		myhr=hr;
	}
if($scope.campm=='am' && myhr==12){
		 
		
		
		myhr="00";
	}
	TodayDate = new Date();

	var evntdate= new Date($scope.Event.eventDate);
	
	console.log(myhr);
	;
	console.log($scope.cmin.min);
	$scope.Event.eventTime=myhr+':'+$scope.cmin.min;
	
	console.log(TodayDate);
	console.log(TodayDate.getTime());
	console.log(evntdate.getTime());
	
	var hours1   = TodayDate.getHours();
	  var minutes = TodayDate.getMinutes();
	  console.log(TodayDate.getHours());
	  console.log(TodayDate.getMinutes());
	  
	  
		console.log(myhr);
	console.log($scope.cmin.min);
	
	var sign = (TodayDate.getTimezoneOffset() > 0) ? "-" : "+";
    var offset = Math.abs(TodayDate.getTimezoneOffset());
    var hours = pad(Math.floor(offset / 60));
    var minutes = pad(offset % 60);
    console.log(sign + hours + ":" + minutes);
    var timeOffSet=sign + hours + ":" + minutes;
    TodayDate.setHours(0,0,0,0);
    console.log(TodayDate.getTime());
    console.log(evntdate.getTime());
    console.log(evntdate);
    console.log($scope.Event.eventDate);
    var myDate=$scope.Event.eventDate.split("-");
    var newDate=new Date(myDate[0],myDate[1]-1,myDate[2]);
    console.log(newDate);
    console.log(newDate.getTime());
   // console.log(new Date(newDate));
    console.log(TodayDate.getTime());
    console.log(newDate.getTime());
    console.log(hours1);
    console.log(myhr); 
    console.log(minutes);
    console.log($scope.cmin.min);
	if (TodayDate.getTime() == newDate.getTime() && hours1>=myhr && minutes>=$scope.cmin.min) {
		
		
		
		/*if($scope.event.eventDate==null){
		
		$scope.error = "Event date needs to be more than todays date";
		}*/
		console.log($scope.Event.eventDate);
		$scope.c_error = "Event date needs to be more than todays date";
		
		
	} else{
		
		
	
	
	console.log(angular.toJson($scope.questionelemnt));
console.log($scope.Event.eventTime);
console.log("$scope.event.eventTime");
console.log($scope.questionelemnt);
data=[];
var error=false;
angular.forEach($scope.questionelemnt, function(value, key){
var nerror=false;
  var eperror=false;
console.log(value.name);
if(value.name=='')
	{
	console.log("Name is blank");
	
	var nerror=true;
	
	}

   console.log(value.name);
   console.log(value.phone);
   if(value.email=='' && (value.phone=='' || value.phone==null)){
	  
	  var eperror=true;
   }
	  if(!(nerror==true && eperror==true)){
		  console.log("inside first loop");
		  if(nerror==true ){
			  $scope.c_error = "Invitee name can not be blank";
			  console.log("inside second loop");
			  error=true;
				
			  return;
		  }else{
			  console.log(value.email);
			  console.log(value.phone);
			  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			  if(!(value.email=='' || value.email==null)){
			  if((!(value.email=='')) && !re.test(value.email)){
				  console.log("inside email fail");
				  eperror=true;
			  }
			  }else{
				  value.email=null;
			  }
			  if(!(value.phone=='' || value.phone==null)){
			  if (!/^[0-9]{10}$/.test(value.phone)) {
				  console.log("inside phone fail");
				  eperror=true;
			  }
			  }else{
				  value.phone=null;
			  }
			  
			  if(eperror==true ){
				  console.log(value.email);
				  console.log(value.phone);
				  
				  $scope.c_error = "Invitee valid email or valid phone(10 digit) is mandatory ";
				  console.log("inside third loop");
				  
				  error=true;
					
    			  return;
    		  }
			 
			  console.log("invitee passed");
			  data.push({"email":value.email,"name":value.name,"phone":value.phone});
		  }
		  }
			  
		  
		  
	  
   	
});

console.log(data);
var userid= $sessionStorage.user.userId;
if(!error){
	
	  var req1= {
		        "userId": userid,
		        "eventName": $scope.Event.eventName,
		        "eventDate": $scope.Event.eventDate,
		        "eventTime": $scope.Event.eventTime,
		        "timeOffSet":timeOffSet,
		        "participants":data
	};
console.log('reqpare'+angular.toJson(req1));

	/*alert("in function");
	alert(angular.toJson($scope.user));*/
	$http({
        method : 'POST',
        url : "./rest/createEvent",
        data : req1,

	}).success(function (data, status, headers, config) {
	//	console.log(angular.toJson(data));
	//	$scope.user=angular.fromJson(data);
		$scope.c_success="Event created Successfully";
		$scope.collapsed=false;
		console.log(status);
		$sessionStorage.user=$scope.user;
		//alert(status);
		console.log($sessionStorage.user);
		
		 var currentDate = new Date();
    	 var day1 = currentDate.getDate();
    	 var month1 = currentDate.getMonth() + 1;
    	var day=pad(day1);
    	var month=pad(month1);
    	 var year = currentDate.getFullYear();
    	 var today = year+'/'+month+'/'+day;
    	 console.log('today'+today);
    	
    	 
   	 var nextday=getTomorrow();
   	 var futureday=getsixmonth();
   	 function getTomorrow() {
   		    const tomorrow = new Date();
   		    tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
   		    var mont=tomorrow.getMonth() + 1;
   		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
   		    return tmrwdate;
   		};
   		 function getsixmonth() {
    		    const tomorrow = new Date();
    		    tomorrow.setDate(tomorrow.getDate() + 100); // even 32 is acceptable
    		   var mont=tomorrow.getMonth() + 1;
    		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
    		    return tmrwdate;
    		};
   	console.log('nextday'+nextday);
   	console.log('futureday'+futureday);
   	
   		 loaddata(today,today);
   		 loaddatafuture(nextday,futureday);
   	
   		
   	

		
		/*alert("after submit");
        alert(status);*/
    })
    .error(function (data, status, headers, config) {
    	//console.log(angular.toJson(data));
    	console.log(status);
    	
    	if(status==403)
		   {
		  $sessionStorage.loggedin=false;
		   $scope.c_error="Please Login to continue";
		   $location.path("/otherwise");

		   }else{
			   if(data ==undefined){
				   $scope.c_error = "System Error. Please try again";
		   }else{ if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.c_error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.c_error = data.errorMessage;
		     }}
		   }
    	
    	
    	
    		
    //   var response=data;

     //  console.log(angular.toJson(response));
    });



};
	}
	}

}
};
	
	
	
	
	
	
	
	
	
	
	
	
/*	
$scope.canceledit=function()
{
	
	$scope.class1="linkenable";
	
	}*/


	$scope.editevent=function(eventid){
	
		
		 var userid=$sessionStorage.user.userId;
		var eventTime;
		$scope.error=undefined;
		$scope.success=undefined;
   	var noerror=false;
   	var error=false;
   	
   	var or_date=$scope.editEvent.eventDate;
   	
   	ordate=or_date+'';
   	
   	if (!(angular.isDefined($scope.editEvent.eventName)) ){
   	    
   	    $scope.error = "Please fill Event Name, date and time";
   	    noerror=true;
   		
   	  }
   	
   	
   	if (!(angular.isDefined($scope.editEvent.eventName))) {
   		$scope.error = "Please fill Event Name, date and time";
   		noerror=true;
   		
   	  }
   	if (!(angular.isDefined($scope.min))) {
   		$scope.error = "Please fill Event Name, date and time";
   		noerror=true;
   		
   	  }
   	if (!(angular.isDefined($scope.hour))) {
   		$scope.error = "Please fill Event Name, date and time";
   		noerror=true;
   		
   	  }
   	if (!(angular.isDefined($scope.editEvent.eventDate))) {
   		$scope.error = "Please fill Event Name, date and time";
   		noerror=true;
   	
   	  }
   	console.log('$scope.editEvent.eventType'+$scope.editEvent.eventType);
   	if($scope.editEvent.eventType=='gmail'){
			$scope.error="Google Event cann't be editted";
			
		return;		
		}
   	
   	if(!noerror){
   	console.log(noerror);
   	
   	if(angular.equals({}, $scope.editEvent.eventName)){
   		console.log("eventname is blank");
   	}else{
   		var hr;

		console.log('hour'+this.hour.hour);
		console.log('hour'+$scope.hour);
		 hr= parseInt(this.hour.hour, 10); 
		 if(this.hour.hour==undefined){
			 hr= parseInt($scope.hour, 10); 
		}
		var myhr;
		
			myhr= this.hour.hour; 
		
		if(this.hour.hour==undefined){
			myhr= $scope.hour; 
		}
		console.log(this.ampm);
		if(this.ampm=='pm' && myhr!=12){
			 
			
			console.log(hr);
			console.log(hr+12);
			hr=hr+12;
			myhr=hr;
		}
if(this.ampm=='am' && myhr==12){
			 
			
			
			myhr="00";
		}
		TodayDate = new Date();

		var evntdate= new Date($scope.editEvent.eventDate);
		
		console.log(myhr);
		;
		console.log($scope.min.min);
		var min;
		if(this.min.min==undefined){
			 min=$scope.min;
		}else{
			 min=this.min.min;
		}
		
		
		 eventTime=myhr+':'+min;
		
		console.log(TodayDate.getTime());
		console.log(evntdate.getTime());
		
		var hours1   = TodayDate.getHours();
		
		
		var sign = (TodayDate.getTimezoneOffset() > 0) ? "-" : "+";
	    var offset = Math.abs(TodayDate.getTimezoneOffset());
	    var hours = pad(Math.floor(offset / 60));
	    var minutes = pad(offset % 60);
	    console.log(sign + hours + ":" + minutes);
	    var timeOffSet=sign + hours + ":" + minutes;
	    TodayDate.setHours(0,0,0,0);
	   
	    $scope.original_date=$scope.original_date+'T00:00:00';
	   
	    var event_date=new Date($scope.original_date.replace(/-/g, '\/').replace(/T.+/, ''));
	   
	    var current=new Date();
	    current.setHours(0,0,0,0);
	    event_date.setHours(0,0,0,0);
	  
	    if(current>event_date){
	    	$scope.error = "Event date has been passed, can't edit the event";
	    	
	    	return;
	    }
	    
	    var myDate=$scope.editEvent.eventDate.split("-");
	    var newDate=new Date(myDate[0],myDate[1]-1,myDate[2]);
	   
	  
	 
	    if(TodayDate.getTime() == newDate.getTime() && hours1==myhr &&  minutes>=min){
			noerror = true;
			
			
			
			$scope.error = "Event date needs to be more than todays date";	
	   		  
			return;
			}
if (TodayDate.getTime() == newDate.getTime() && hours1>=myhr ) {
			
			noerror = true;
			
			
			
			$scope.error = "Event date needs to be more than todays date";
		
			
}
			
	
		
		
else{
			
		
		
		
console.log(eventTime);
console.log("eventTime");
data=[];

angular.forEach($scope.editEvent.participants, function(value, key){
	var nerror=false;
	  var eperror=false;
   console.log(value.name);
   if(value.name=='' || value.name==null)
   	{
   	console.log("Name is blank");
   	nerror=true;
   	
   	}
   
      console.log(value.name);
      console.log(value.phone);
      console.log(value.email);
    
      if((value.email=='' || value.email==null)&& (value.phone=='' || value.phone==null) ){
   	  
   	  eperror=true;
      }
   	  if((nerror==false && eperror==false)){
   		  console.log("inside first loop");
   		  if(nerror==true ){
   			  $scope.error = "Invitee name can not be blank";
   			  console.log("inside second loop");
   			  error=true;
   			
   		  }else{
   			  console.log(value.email);
				  console.log(value.phone);
				  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				  if(!(value.email=='' || value.email==null)){
				  if((!(value.email=='')) && !re.test(value.email)){
					  console.log("inside email fail");
					  eperror=true;
				  }
				  }
				  else{
					  value.email=null;
				  }
				  
				 
				 
				  if(!(value.phone=='' || value.phone==null)){
				  if (!/^[0-9]{10}$/.test(value.phone)) {
					  console.log("inside phone fail");
					  eperror=true;
				  }
				  }else{
					  value.phone=null;
				  }
				  
   			  if(eperror==true ){
   				  console.log(value.email);
   				  console.log(value.phone);
   				  
   				  $scope.error = "Invitee valid email or valid phone(10 digit) is mandatory ";
   				  console.log("inside third loop");
   			
   				  
   				  error=true;
       			 
       		  }
   			  console.log("invitee passed");
   			  data.push({"email":value.email,"name":value.name,"phone":value.phone});
   		  }
   		  }else{
   			  $scope.error = "Please enter invitee details ";
   			
			
   		  }
   			  
   		  
   		  
   	  
      	
});

console.log('error'+error);

if(!error){
		  var req1= {
				  "userId":userid,
			        "eventId": eventid,
			        "eventName": $scope.editEvent.eventName,
			        "eventDate": $scope.editEvent.eventDate,
			        "eventTime": eventTime,
			        "timeOffSet":timeOffSet,
			        "participants":data
		};
console.log('reqpare'+angular.toJson(req1));
		/*alert("in function");
		alert(angular.toJson($scope.user));*/
$http({
   method : 'PUT',
   url : "./rest/updateevent",
   data : req1,

}).success(function (data, status, headers, config) {
//	console.log(angular.toJson(data));
//	$scope.user=angular.fromJson(data);
	console.log(status);
	//$sessionStorage.user=$scope.user;
	//alert(status);
//	console.log($sessionStorage.user);
	//$location.path("/home");
	$scope.error=undefined;
	$scope.success="Event saved Successfully";
	
	 $scope.opened=false;
	
	 var currentDate = new Date();
	 var day1 = currentDate.getDate();
	 var month1 = currentDate.getMonth() + 1;
	var day=pad(day1);
	var month=pad(month1);
	 var year = currentDate.getFullYear();
	 var today = year+'/'+month+'/'+day;
	 console.log('today'+today);
	
	 
	// $scope.fromdate=today;
	// $scope.todate=today;
	 var nextday=getTomorrow();
	 var futureday=getsixmonth();
	 function getTomorrow() {
		    const tomorrow = new Date();
		    tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
		    var mont=tomorrow.getMonth() + 1;
		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
		    return tmrwdate;
		};
		 function getsixmonth() {
 		    const tomorrow = new Date();
 		    tomorrow.setDate(tomorrow.getDate() + 100); // even 32 is acceptable
 		   var mont=tomorrow.getMonth() + 1;
 		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
 		    return tmrwdate;
 		};
	console.log('nextday'+nextday);
	console.log('futureday'+futureday);
	
		 loaddata(today,today);
		 loaddatafuture(nextday,futureday);
		
		

	/*alert("after submit");
   alert(status);*/
})
.error(function (data, status, headers, config) {
	//console.log(angular.toJson(data));
	console.log(status);
	
	if(status==403)
	   {
	  $sessionStorage.loggedin=false;
	   $scope.error="Please Login to continue";
	   $location.path("/otherwise");

	   }else{
		   if(data ==undefined){
			   $scope.error = "System Error. Please try again";
	   }else{ 
		   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
	    	 $scope.error = "System Error. Please try again";
	     }
	     else{
	    	 $scope.error = data.errorMessage;
	     }
		   }
	
	   }
	
	
		
//  var response=data;

//  console.log(angular.toJson(response));
});



	};
   	}
   	}
	
   }
   	
	};


	
	
	
	
	
	
	
	$scope.class1=undefined;
	
	$scope.canceldialogue=function(){
	 $scope.opened=false;
	
	 var currentDate = new Date();
	 var day1 = currentDate.getDate();
	 var month1 = currentDate.getMonth() + 1;
	var day=pad(day1);
	var month=pad(month1);
	 var year = currentDate.getFullYear();
	 var today = year+'/'+month+'/'+day;
	 console.log('today'+today);
	
	 
	// $scope.fromdate=today;
	// $scope.todate=today;
	 var nextday=getTomorrow();
	 var futureday=getsixmonth();
	 function getTomorrow() {
		    const tomorrow = new Date();
		    tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
		    var mont=tomorrow.getMonth() + 1;
		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
		    return tmrwdate;
		};
		 function getsixmonth() {
		    const tomorrow = new Date();
		    tomorrow.setDate(tomorrow.getDate() + 100); // even 32 is acceptable
		   var mont=tomorrow.getMonth() + 1;
		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
		    return tmrwdate;
		};
	console.log('nextday'+nextday);
	console.log('futureday'+futureday);
	
		 loaddata(today,today);
		 loaddatafuture(nextday,futureday);
		
}
	
	
$scope.getevent=function(eventid)
{
	
	$scope.class1="linkDisabled";
	$scope.error=undefined;
	$scope.success=undefined;
	
	$scope.opened=true;

	TodayDate = new Date();

	
	var sign = (TodayDate.getTimezoneOffset() > 0) ? "-" : "+";
    var offset = Math.abs(TodayDate.getTimezoneOffset());
    var hours = pad(Math.floor(offset / 60));
    var minutes = pad(offset % 60);
    console.log(sign + hours + ":" + minutes);
    var timeOffSet=sign + hours + ":" + minutes;
    timeOffSet=encodeURIComponent(timeOffSet);



$scope.editEvent={};
$http({
    method : 'GET',
    url : './rest/geteventdetails?eventid='+eventid+'&timeOffSet='+timeOffSet ,     
   headers: {'Content-Type': 'application/x-www-form-urlencoded'}
}).success(function (data, status, headers, config) {
	console.log(status);

$scope.editEvent=data;


$scope.original_date=$scope.editEvent.eventDate;

         var eventtime = $scope.editEvent.eventTime;
         console.log(eventtime);
         
         console.log(eventtime.substring(0, 2));
         var eventhours=parseInt(eventtime.substring(0, 2));
         console.log(eventhours);
         var hours = eventhours == 0 ? "12" : eventhours> 12 ? eventhours - 12 : eventhours;
         var minutes=eventtime.substring(3, 6);
         console.log(minutes);
         /* var minutes = (eventtime.getMinutes() < 10 ? "0" : "") + eventtime.getMinutes();*/
         var ampm = eventhours < 12 ? "am" : "pm";
         var formattedTime = hours  + eventtime.substring(3, 6) + " " + ampm;
         console.log('formattedTime'+formattedTime);
         $scope.editEvent.eventTime=formattedTime;
         var sHours = hours.toString();
         var sMinutes = minutes.toString();
         if (hours < 10) sHours = "0" + sHours;
         if (minutes < 10) sMinutes = "0" + sMinutes;
         
         
       $scope.hour=sHours;
       $scope.min=eventtime.substring(3, 6);
       $scope.ampm=ampm;
         
         
    
})
.error(function (data, status, headers, config) {
	console.log(status);
	if(status==403)
	   {
	  $sessionStorage.loggedin=false;
	   $scope.error="Please Login to continue";
	   $location.path("/otherwise");

	   }else{
		   if(data ==undefined){
			   $scope.error = "System Error. Please try again";
	   }else{ 
		   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
	    	 $scope.error = "System Error. Please try again";
	     }
	     else{
	    	 $scope.error = data.errorMessage;
	     }
		   }
		  
	   }
	
	
});
var counter=0;

$scope.newItem = function($event){
    counter++;
    if($scope.editEvent.participants==undefined){
    	$scope.editEvent.participants=  [{email : '',name : '',phone : ''}] ;
    }
    else{
    $scope.editEvent.participants.push(  {email : '',name : '',phone : ''} );
    }
    $event.preventDefault();
};



};
	
	
	
	
	
	
	
	function pad(value) {
	    return value < 10 ? '0' + value : value;
	}
	
	$scope.username=$sessionStorage.user.name;
	var userId=$sessionStorage.user.userId;
	
	
		
			$scope.toggleMin = function() {
			    var date = new Date();
			    $scope.minDate = date.setDate((new Date()).getDate());
			    console.log($scope.minDate);
			  };
			  $scope.toggleMin();

			
				
			
		 

	angular.element(document).ready(function () {
		 $scope.error=undefined;
		 $scope.success=undefined;
		 $scope.info=undefined;
	    console.log('page loading completed');
	    $scope.noedit=true;
	    $scope.opened=false;
		var userid=$sessionStorage.user.userId;
		$scope.username=$sessionStorage.user.name;
	    console.log(userid);
	
	    /*console.log($sessionStorage.usertype);*/
	    $scope.collapsed=false;
    	
    	var currentDate = new Date();
    	var day1 = currentDate.getDate();
    	var month1 = currentDate.getMonth() + 1;
    	var day=pad(day1);
    	var month=pad(month1);
    	var year = currentDate.getFullYear();
    	var today = year+'/'+month+'/'+day;
    	console.log('today'+today);
    	
    	 
    	// $scope.fromdate=today;
    	// $scope.todate=today;
    	 
    	 var nextday=getTomorrow();
    	 var futureday=getsixmonth();
    	 function getTomorrow() {
    		    const tomorrow = new Date();
    		    tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
    		    var mont=tomorrow.getMonth() + 1;
    		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
    		    return tmrwdate;
    		};
    		 function getsixmonth() {
     		    const tomorrow = new Date();
     		    tomorrow.setDate(tomorrow.getDate() + 100); // even 32 is acceptable
     		   var mont=tomorrow.getMonth() + 1;
     		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
     		    return tmrwdate;
     		};
    	console.log('nextday'+nextday);
    	console.log('futureday'+futureday);
    	
    		 loaddata(today,today);
    		 loaddatafuture(nextday,futureday);
    	
    	
    	$scope.ampm = (new Date().getHours()) >= 12 ? 'pm' : 'am';
		
		
		$scope.minutes=[{min:'00',id:'1'},{min:'01'},{min:'02'},{min:'03'},{min:'04'},{min:'05'},{min:'06'},{min:'07'},{min:'08'},{min:'09'},{min:'10'},{min:'11'},{min:'12'},{min:'13'},{min:'14'},{min:'15'},{min:'16'},{min:'17'},{min:'18'},{min:'19'},{min:'20'},{min:'21'},{min:'22'},{min:'23'},{min:'24'},{min:'25'},{min:'26'},{min:'27'},{min:'28'},{min:'29'},{min:'30'},{min:'31'},{min:'32'},{min:'33'},{min:'34'},{min:'35'},{min:'36'},{min:'37'},{min:'38'},{min:'39'},{min:'40'},{min:'41'},{min:'42'},{min:'43'},{min:'44'},{min:'45'},{min:'46'},{min:'47'},{min:'48'},{min:'49'},{min:'50'},{min:'51'},{min:'52'},{min:'53'},{min:'54'},{min:'55'},{min:'56'},{min:'57'},{min:'58'},{min:'59'}];
		$scope.hours=[{hour:'01'},{hour:'02'},{hour:'03'},{hour:'04'},{hour:'05'},{hour:'06'},{hour:'07'},{hour:'08'},{hour:'09'},{hour:'10'},{hour:'11'},{hour:'12'}];
	 
		
		console.log('page loading completed');
	    console.log($sessionStorage.usertype);
	   
	    
	    $scope.hour='00';
	    $scope.min='00';
	    console.log($scope.hour);
	    console.log($scope.hour);	

	});
	
	
	
	
	
	
	//var startdate=$scope.fromdate;
	//var enddate=$scope.todate;
	//var userid=3;
	
	
	 function loaddatafuture(startdate,enddate){
		 //$scope.loaddata=function(startdate,enddate){
			 
			 TodayDate = new Date();

				
				var sign = (TodayDate.getTimezoneOffset() > 0) ? "-" : "+";
			    var offset = Math.abs(TodayDate.getTimezoneOffset());
			    var hours = pad(Math.floor(offset / 60));
			    var minutes = pad(offset % 60);
			    console.log(sign + hours + ":" + minutes);
			    var timeOffSet=sign + hours + ":" + minutes;
			    timeOffSet=encodeURIComponent(timeOffSet);
		$http({
		    method : 'GET',
		    url : './rest/getevent?userid='+userid+''+'&startdate='+startdate+'&enddate='+enddate+'&timeOffSet='+timeOffSet,
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    var response=data;
		    console.log(response);
		    console.log("response above");
		    Object.keys(response).length;
		    console.log(Object.keys(response).length);
		    //var userpresence=response.userExist;
		  //  $scope.user=response;

		    if(Object.keys(response).length==0 || response.events.length==0){
		    	console.log("blank response");
		    	$('#nodata').css('display','block');
		    	$scope.BindJsonDetailsfuture = {};
		    }else{
		    	$('#nodata').css('display','none');
		    $scope.BindJsonDetailsfuture = data;
		    console.log(data);
		    console.log(data.events);
		    
		    for(var i in data.events)
		    {
		         var eventtime = data.events[i].eventTime;
		         console.log(eventtime);
		         
		         console.log(eventtime.substring(0, 2));
		         var eventhours=parseInt(eventtime.substring(0, 2));
		         console.log(eventhours);
		         var hours = eventhours == 0 ? "12" : eventhours> 12 ? eventhours - 12 : eventhours;
		         var minutes=eventtime.substring(2, 6);
		         console.log(minutes);
		         /* var minutes = (eventtime.getMinutes() < 10 ? "0" : "") + eventtime.getMinutes();*/
		         var ampm = eventhours < 12 ? "am" : "pm";
		         var formattedTime = hours  + eventtime.substring(2, 6) + " " + ampm;
		         console.log(formattedTime);
		         data.events[i].eventTime=formattedTime;
		         
		         
		    }
			
		}
		   


		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		   console.log("inside the failure");
		   
		   if(status==403)
		   {
		  $sessionStorage.loggedin=false;
		   $scope.error="Please Login to continue";
		   $location.path("/otherwise");

		   }else{
			   if(data ==undefined){
				   $scope.error = "System Error. Please try again";
		   }else{ 
			   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
		    	 $scope.error = "System Error. Please try again";
		     }
		     else{
		    	 $scope.error = data.errorMessage;
		     }
			   }
		   }
			
			   
		   
		   
		});
	 }

	 function loaddata(startdate,enddate){
	 //$scope.loaddata=function(startdate,enddate){
		 $scope.BindJsonDetails =undefined;
		
		 TodayDate = new Date();

			
			var sign = (TodayDate.getTimezoneOffset() > 0) ? "-" : "+";
		    var offset = Math.abs(TodayDate.getTimezoneOffset());
		    var hours = pad(Math.floor(offset / 60));
		    var minutes = pad(offset % 60);
		    console.log(sign + hours + ":" + minutes);
		    var timeOffSet=sign + hours + ":" + minutes;
		    timeOffSet=encodeURIComponent(timeOffSet);
	$http({
	    method : 'GET',
	    url : './rest/getevent?userid='+userid+''+'&startdate='+startdate+'&enddate='+enddate+'&timeOffSet='+timeOffSet,
	    headers: { 'Content-Type':'application/json;charset=UTF-8'}
	}).success(function (data, status, headers, config) {


	    var response=data;
	    console.log(response);
	    console.log("response above");
	    Object.keys(response).length;
	    console.log(Object.keys(response).length);
	    //var userpresence=response.userExist;
	  //  $scope.user=response;

	    if(Object.keys(response).length==0 || response.events.length==0){
	    	console.log("blank response");
	    	$('#nodata').css('display','block');
	    	$scope.BindJsonDetails = {};
	    }else{
	    	$('#nodata').css('display','none');
	    $scope.BindJsonDetails = data;
	    console.log(data);
	    console.log(data.events);
	    
	    for(var i in data.events)
	    {
	         var eventtime = data.events[i].eventTime;
	         console.log(eventtime);
	         
	         console.log(eventtime.substring(0, 2));
	         var eventhours=parseInt(eventtime.substring(0, 2));
	         console.log(eventhours);
	         var hours = eventhours == 0 ? "12" : eventhours> 12 ? eventhours - 12 : eventhours;
	         var minutes=eventtime.substring(2, 6);
	         console.log(minutes);
	         /* var minutes = (eventtime.getMinutes() < 10 ? "0" : "") + eventtime.getMinutes();*/
	         var ampm = eventhours < 12 ? "am" : "pm";
	         var formattedTime = hours  + eventtime.substring(2, 6) + " " + ampm;
	         console.log(formattedTime);
	         data.events[i].eventTime=formattedTime;
	         
	         
	    }
		 $sessionStorage.fromdate_cache=$scope.fromdate;
    	 $sessionStorage.todate_cache=$scope.todate;
	}
	   


	})
	.error(function (data, status, headers, config) {
	   console.log(status);
	   console.log("inside the failure");
	   
	   if(status==403)
	   {
	  $sessionStorage.loggedin=false;
	   $scope.error="Please Login to continue";
	   $location.path("/otherwise");

	   }else{
		   if(data ==undefined){
			   $scope.error = "System Error. Please try again";
	   }else{ 
		   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
	    	 $scope.error = "System Error. Please try again";
	     }
	     else{
	    	 $scope.error = data.errorMessage;
	     }
		   }
	   }
	   
		
	
	 
	   
	});
	}

	console.log($sessionStorage.user);
	console.log($scope.BindJsonDetails);
	
   $scope.inlinef= function($event,inlinecontrol){
       var checkbox = $event.target;
       if(checkbox.checked){
           $('#'+ inlinecontrol).css('display','inline');
       }else{
           $('#'+ inlinecontrol).css('display','');
       }

   }
   $scope.showitems = function($event){
       $('#displayitems').css('visibility','none');
   }
  
   $scope.eventtype=function(value){
	  if(value=='gmail'){
		  return false;
	  }
	  else {
		  return true;
	  }
   };
   
  
   
   $scope.submitdates = function(){
       console.log("in submit dates");
       console.log('$scope.fromdate'+$scope.fromdate);
       console.log('$scope.todate'+$scope.todate);
       if ($scope.fromdate>$scope.todate) {
			
			
			$scope.error = "To date needs to be more than from date";
		 return;
			
        }
       
       
       loaddatafuture($scope.fromdate,$scope.todate);
   }
   
   
 $scope.showinfopop=function(eventid){
	 $scope.currentevent=eventid;
	 $scope.info=true;
 };
   
   
   $scope.deleteevent = function(eventid){
	  $scope.info=false;
	   
       console.log(eventid);
       console.log("in delete events");
     
      
         	$http({
   	    method : 'DELETE',
   	    url : './rest/deleteevent?eventid='+eventid,
   	    headers: { 'Content-Type':'application/json;charset=UTF-8'}
   	}).success(function (data, status, headers, config) {

   		console.log(status);
   		$scope.success='Successfully Deleted Event..';
   		
   	 var currentDate = new Date();
	 var day1 = currentDate.getDate();
	 var month1 = currentDate.getMonth() + 1;
	var day=pad(day1);
	var month=pad(month1);
	 var year = currentDate.getFullYear();
	 var today = year+'/'+month+'/'+day;
	 console.log('today'+today);
	
	 
	// $scope.fromdate=today;
	// $scope.todate=today;
	 var nextday=getTomorrow();
	 var futureday=getsixmonth();
	 function getTomorrow() {
		    const tomorrow = new Date();
		    tomorrow.setDate(tomorrow.getDate() + 1); // even 32 is acceptable
		    var mont=tomorrow.getMonth() + 1;
		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
		    return tmrwdate;
		};
		 function getsixmonth() {
 		    const tomorrow = new Date();
 		    tomorrow.setDate(tomorrow.getDate() + 100); // even 32 is acceptable
 		   var mont=tomorrow.getMonth() + 1;
 		    tmrwdate=tomorrow.getFullYear()+'/'+mont+'/'+tomorrow.getDate();
 		    return tmrwdate;
 		};
	console.log('nextday'+nextday);
	console.log('futureday'+futureday);
	
		 loaddata(today,today);
		 loaddatafuture(nextday,futureday);
   	   


   	})
   	.error(function (data, status, headers, config) {
   	   console.log(status);
   	   console.log("inside the failure");
   	if(status==403)
	   {
	  $sessionStorage.loggedin=false;
	   $scope.error="Please Login to continue";
	   $location.path("/otherwise");

	   }else{
		   if(data ==undefined){
			   $scope.error = "System Error. Please try again";
	   }else{ 
		   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
	    	 $scope.error = "System Error. Please try again";
	     }
	     else{
	    	 $scope.error = data.errorMessage;
	     }
		   }  
	   }
	

   	});
   	
   
   	
   }
  
  
	$scope.user=$sessionStorage.user;
	console.log($scope.user);
	
	$scope.IsVisible1 = false;

   $scope.ShowHide1 = function () {
       //If DIV is visible it will be hidden and vice versa.
       $scope.IsVisible1 = $scope.IsVisible1 ? false : true;
   };

	/*alert("before submit");*/
  
   
   
 var counter=0;  
   $scope.editnewItem = function($event){
       counter++;
       if($scope.editEvent.participants==undefined){
       	$scope.editEvent.participants=  [{email : '',name : '',phone : ''}] ;
       }
       else{
       $scope.editEvent.participants.push(  {email : '',name : '',phone : ''} );
       }
       $event.preventDefault();
   };
   
   
	
	}]);


mainApp.controller('userController', ['$scope','$http','$sessionStorage','$location','$window','$log', function($scope,$http,$sessionStorage,$location,$window,$log){
	$scope.mnemepolicy=function(){
		$scope.info3=true;	
		};
		$scope.mnemecontact=function(){
			$scope.info4=true;	
			};
	$sessionStorage.cronofy=false;
	$sessionStorage.eventNavigate=false;
	$sessionStorage.fromdate_cache=undefined;
	$sessionStorage.todate_cache=undefined;
	  $scope.user=$sessionStorage.user;
	    $scope.username=$sessionStorage.user.name;
	    /*console.log($sessionStorage.usertype);*/
	var fromauth=false;
	$scope.logout = function(){
		
		
		
		$http({
		    method : 'POST',
		    url : './rest/logout',
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {


		    
		    //var userpresence=response.userExist;
		   // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
		    
		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  // $scope.user=response;
		    $sessionStorage.loggedin=false;
		    $sessionStorage.user=undefined;
		    $sessionStorage.usertype=false;
		    $sessionStorage.usertype=null;
		     $window.location.href=(".#/otherwise");
			   

			 
		});
	
		};
		
	$scope.login = function(){
		/*alert("in function");
		alert(angular.toJson($scope.user));*/
		$window.location.href=(".#/login.html");
		};
			 
			 angular.element(document).ready(function () {
				 
				    console.log('page loading completed');
				    $scope.user=$sessionStorage.user;
				    $scope.username=$sessionStorage.user.name;
				    /*console.log($sessionStorage.usertype);*/
				   

				});
			
	 
	console.log($sessionStorage.user);

	$scope.user=$sessionStorage.user;
	console.log($scope.user);
	if($scope.user==null){
		$location.path("/otherwise");

	}
	else
		{
		var userid=$sessionStorage.user.userId;
		$http({
		    method : 'GET',
		    url : './rest/user/'+userid,
		    headers: { 'Content-Type':'application/json;charset=UTF-8'}
		}).success(function (data, status, headers, config) {

console.log(angular.toJson(data));
		    var response=data;
		    //var userpresence=response.userExist;
		    $scope.user=response;
		   
		    
		    if($scope.user.name==undefined && $scope.user.gmailUser){
				$scope.user.name=$scope.user.login;
				}
				

		})
		.error(function (data, status, headers, config) {
		   console.log(status);
		  
		   if(status==403)
			   {
			   $sessionStorage.loggedin=false;
			   $scope.error="Please Login to continue";
			   $location.path("#/otherwise");

			   }else {
				 
				   if(data ==undefined){
					   $scope.error = "System Error. Please try again";
			   }else{ 
				   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
			    	 $scope.error = "System Error. Please try again";
			     }
			     else{
			    	 $scope.error = data.errorMessage;
			     }
				   }
			   }
		});
		}
	$scope.IsVisible1 = false;

    $scope.ShowHide1 = function () {
        //If DIV is visible it will be hidden and vice versa.
    	console.log($scope.user.accessToGoogleCal);
    	if($scope.user.accessToGoogleCal==true){
    		if($scope.IsVisible1==false){
        $scope.IsVisible1 =  true;
    		}
    	}
    	
    	if($scope.user.accessToGoogleCal==false){
    		if($scope.IsVisible1==true){
    	        $scope.IsVisible1 =  false;
    	    		}
        	}
    };

	/*alert("before submit");*/

    
    $scope.submitauth=function(){
    	
    	fromauth=true;
    	console.log('fromauth insde auth'+fromauth);
    	$scope.submitForm();
    	$sessionStorage.cronofy=true;
    	
    	var userid=$sessionStorage.user.userId;
    	        if(!$scope.error){
    	        	$sessionStorage.cronofy=true;
        	
    	        	$window.location.href='https://app.cronofy.com/oauth/authorize?response_type=code&client_id=rCT7ZMbXgdzNqcceKoZ522JadbyzVmnm&redirect_uri=https://app.mnemeinc.com/Mneme/&scope=read_events&provider_name=google&avoid_linking=true';
    	        	//$window.location.href='https://app.cronofy.com/oauth/authorize?response_type=code&client_id=rCT7ZMbXgdzNqcceKoZ522JadbyzVmnm&redirect_uri=http://localhost:8080/Mneme/&scope=read_events&provider_name=google&avoid_linking=true';
        	
        	//$window.location.href='https://app.cronofy.com/oauth/authorize?response_type=code&client_id=ZP-mQTIkuYdWuWlzXy9rZxrpUsUN7XTF&redirect_uri=http://mneme-app.us-west-2.elasticbeanstalk.com/Mneme/&scope=read_events&provider_name=google&avoid_linking=true';
    //    	$window.location.href="https://www.google.co.in/?gfe_rd=cr&ei=dEnSV-XzO-bI8AfF4KLYDg";
      //  var url=$window.location.href;
       // console.log(url);
        
        }     
        
    	
    }
	$scope.submitForm = function(){
		
		/*start of code by shamilee*/
		$scope.error=undefined;
		$scope.success =undefined;
		
		
		if($scope.user==undefined){
			$scope.error='Please enter user info ';
			return;
		}
			
			if($scope.user.name==undefined){
		
				$scope.error='user name is mandatory ';
							return;
			
			}
	if(!($scope.user.phoneNumber=='' || $scope.user.phoneNumber==null || $scope.user.phoneNumber==undefined)){
		  if (!/^[0-9]{10}$/.test($scope.user.phoneNumber)) {
			  $scope.error='Phone is not valid , please enter 10 digit number ';
				return;
		  }
		  
		  }else{
			  $scope.error='Phone is Mandatory ';
				return;
		  }
				
				
			
			
		
	$scope.error=undefined;		
		
		/*end of code by shamilee*/
	
		/*alert("in function");
		alert(angular.toJson($scope.user));*/
		$http({
            method : 'PUT',
            url : "./rest/updateUser",
            data : angular.toJson($scope.user),

		}).success(function (data, status, headers, config) {
			console.log(angular.toJson(data));
			//$scope.user=angular.fromJson(data);
			console.log($scope.user);
			//$sessionStorage.user=$scope.user;

			console.log($sessionStorage.user);
			$scope.error=false;
			if(fromauth==false){
				$scope.success = "Your Data is saved succesfully";
			}
			fromauth=false;

			//$location.path("/home");

			/*alert("after submit");
            alert(status);*/
        })
        .error(function (data, status, headers, config) {
        	console.log(angular.toJson(data));
        	console.log(status);
        	console.log(status);
        	 
 		   if(status==403)
 			   {
 			  $sessionStorage.loggedin=false;
 			   $scope.error="Please Login to continue";
 			   $location.path("./login.html");

 			   }else{
 				  
 				  if(data ==undefined){
					   $scope.error = "System Error. Please try again";
			   }else{ 
				   if(data.errorMessage==undefined || data.errorMessage == '' || data.errorMessage==null){
			    	 $scope.error = "System Error. Please try again";
			     }
			     else{
			    	 $scope.error = data.errorMessage;
			     }
				   }
 			   }
 		   
           
           var response=data;

           console.log(angular.toJson(response));
        });



	};
	$scope.cancelForm=function(){
		$window.location.href=(".#/event");
	};
	}]);

