package Util;

import com.service.*;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class VerifyRecaptcha {

	public  static String url = null;
	public  static String secret =null;
	private static String USER_AGENT =null;

	public static boolean verify(String gRecaptchaResponse) throws IOException {
		if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
			return false;
		}

		try {
			GetProperties u =new GetProperties();
	         url= u.getPropValues("url");
	        System.out.println(url);
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			
			USER_AGENT= u.getPropValues("USERAGENT");
	        System.out.println(USER_AGENT);
			con.setRequestProperty("User-Agent", USER_AGENT);
			
			String Acceplang= u.getPropValues("AcceptLanguage");
	        System.out.println(Acceplang);
			con.setRequestProperty("Accept-Language", Acceplang);

			secret= u.getPropValues("secret");
	        System.out.println(secret);
			String postParams = "secret=" + secret + "&response="
					+ gRecaptchaResponse;

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(postParams);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + postParams);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			JSONObject jsonObject1 = new JSONObject(response.toString());

			return jsonObject1.getBoolean("success");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
