package com.service;

/**
 * @author Crunchify.com
 *
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import Util.VerifyRecaptcha;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@Path("/")
public class ClientService {

	@POST
	@Path("/createservice")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST(@Context HttpServletRequest httpServletRequest,String incomingData) throws IOException {
		StringBuilder crunchifyBuilder = new StringBuilder();
		JSONObject jsonObject1 = new JSONObject(incomingData);
		String output=null;
		String gRecaptchaResponse = jsonObject1.getString("recaptcha");
		String result = "Captcha Valdiation Failed";

		System.out.println(gRecaptchaResponse);

		boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
		System.out.println(verify);


		if(!verify){
			return Response.status(403).entity(result).build();
		}
		JSONObject user = jsonObject1.getJSONObject("user");


		System.out.println(user);

		GetProperties u =new GetProperties();
        String url= u.getPropValues("createservice");
        System.out.println(url);
			Client client = Client.create();

			WebResource webResource = client
			   .resource(url);
			JSONObject jsonObject = new JSONObject();
			 JSONObject jsonObj = new JSONObject(user);

		//	System.out.println(jsonObj);

			ClientResponse response = webResource.header("Content-Type", "application/json")
			   .post(ClientResponse.class, user.toString());
				System.out.println(response);
			if (response.getStatus() != 200) {
				output = response.getEntity(String.class);
				return Response.status(response.getStatus()).entity(output).build();
			}


			System.out.println("Output from Server .... \n");
			output = response.getEntity(String.class);
			System.out.println(output);
			HttpSession Session = httpServletRequest.getSession();
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);
			NewCookie SessionCookie = new NewCookie("Sessionid", Jsessionid);

		// return HTTP response 200 in case of success
		return Response.status(200).entity(output).cookie(SessionCookie).build();

		// return HTTP response 200 in case of success
	/*	return Response.status(200).entity(output).build();*/
	}


	@POST
	@Path("loginservice")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST_login(@Context HttpServletRequest httpServletRequest,String incomingData)throws IOException {
		String output=null;


			Client client = Client.create();
			GetProperties u =new GetProperties();
	        String url= u.getPropValues("loginservice");
	        System.out.println(url);

			WebResource webResource = client
			   .resource(url);
			JSONObject jsonObject = new JSONObject();
			 JSONObject jsonObj = new JSONObject(incomingData);

			System.out.println(incomingData);
			ClientResponse response = webResource.header("Content-Type", "application/json")
			   .post(ClientResponse.class, jsonObj.toString());
				System.out.println(response);
			if (response.getStatus() != 200) {
				output = response.getEntity(String.class);
				return Response.status(response.getStatus()).entity(output).build();
			}


			System.out.println("Output from Server .... \n");
			output = response.getEntity(String.class);
			System.out.println(output);
			HttpSession Session = httpServletRequest.getSession();
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);
			NewCookie SessionCookie = new NewCookie("Sessionid", Jsessionid);

		// return HTTP response 200 in case of success
		return Response.status(200).entity(output).cookie(SessionCookie).build();
	}

	@POST
	@Path("crunchifyServicelogin1")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST_login1(@Context HttpServletRequest httpServletRequest,String incomingData)throws IOException {
		String output=null;


			Client client = Client.create();
			GetProperties u =new GetProperties();
	        String url= u.getPropValues("loginservice");
	        System.out.println(url);

			WebResource webResource = client
			   .resource(url);
			JSONObject jsonObject = new JSONObject();
			 JSONObject jsonObj = new JSONObject(incomingData);

			System.out.println(incomingData);
			ClientResponse response = webResource.header("Content-Type", "application/json")
			   .post(ClientResponse.class, jsonObj.toString());
				System.out.println(response);
			if (response.getStatus() != 200) {
				output = response.getEntity(String.class);
				return Response.status(response.getStatus()).entity(output).build();
			}


			System.out.println("Output from Server .... \n");
			output = response.getEntity(String.class);
			System.out.println(output);
			HttpSession Session = httpServletRequest.getSession();
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);
			NewCookie SessionCookie = new NewCookie("Sessionid", Jsessionid);

		// return HTTP response 200 in case of success
		return Response.status(200).entity(output).cookie(SessionCookie).build();
	}

	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "CrunchifyRESTService Successfully started..";

		// return HTTP response 200 in case of success
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/user/{userid}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response userdetails(@Context HttpServletRequest httpServletRequest,@PathParam("userid") String userid)throws IOException {
		String result = "CrunchifyRESTService Successfully started..";
 System.out.println(userid);

 String output=null;

 JSONObject jsonObject = new JSONObject();
 HttpSession Session = httpServletRequest.getSession(false);
	System.out.println(Session);

	if (Session == null) {
		System.out.println("Session is null");


		System.out.println(result);
		return Response.status(403).entity(result).build();
		// it's valid
	}
	String Jsessionid = Session.getId();
	System.out.println(Jsessionid);

	Client client = Client.create();
	GetProperties u =new GetProperties();
    String url= u.getPropValues("userdetail");
    System.out.println(url);
	System.out.println("calling service");
	WebResource webResource = client
	   .resource(url+userid);
	/*JSONObject jsonObject = new JSONObject();*/
	 /*JSONObject jsonObj = new JSONObject(incomingData);
	  *
	  *
*/
	/*System.out.println(incomingData);*/
	ClientResponse response = webResource.header("Content-Type", "application/json")
	   .get(ClientResponse.class);
		System.out.println(response);
	if (response.getStatus() != 200) {
		output = response.getEntity(String.class);
		return Response.status(response.getStatus()).entity(output).build();
	}
	System.out.println("calling response" +response );
	output = response.getEntity(String.class);
	System.out.println("calling response" +output );


		return Response.status(200).entity(output).build();
	}


	@GET
	@Path("/auth")
	@Produces(MediaType.TEXT_PLAIN)
	public Response authcronofy(@Context HttpServletRequest httpServletRequest,@QueryParam("userid") String userid,@QueryParam("code") String code) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException,IOException{
		String result = "CrunchifyRESTService Successfully started..";
 System.out.println(code);
 //System.out.println(userid);

 String output=null;

 JSONObject authdata = new JSONObject();
 HttpSession Session = httpServletRequest.getSession(false);
	System.out.println(Session);


 /*{
	  "client_id": "5PBGufThyS3Y4lW4vJZhG8LHtjQElZbs",
	  "client_secret": "zgwW7L_yxMebry8zDoVw3Lc5C8vb0C8Pke8sAYpSmdA1N4m66yfv7mWTaPE8HRicsP96Afymq5Ip4qJnlZGZRw",
	  "grant_type": "authorization_code",
	  "code": "VQym_zAT7KhPwqMKlE602oaJR6dVd4qX",
	  "redirect_uri": "http://localhost:8080/Mneme/rest/auth"
	}*/
	authdata.put("code", code);
	GetProperties u =new GetProperties();
    String clientid= u.getPropValues("clientid");
    authdata.put("client_id",clientid);

    String clientsecret=u.getPropValues("clientsecret");
 authdata.put("client_secret",  clientsecret);

 String redirecturi=u.getPropValues("redirecturi");
 authdata.put("redirect_uri",  redirecturi);

 String granttype=u.getPropValues("granttype");
 authdata.put("grant_type",granttype);

 Client client = null;
	//WebResource webResource = null;
	SSLContext sslContext = null;


// Client client = Client.create();
	InsecureTrustManager secureRestClientTrustManager = new InsecureTrustManager();
	sslContext = SSLContext.getInstance("SSL");
	sslContext
			.init(
					null,
					new javax.net.ssl.TrustManager[] { secureRestClientTrustManager },
					null);

	DefaultClientConfig defaultClientConfig = new DefaultClientConfig();
	defaultClientConfig
			.getProperties()
			.put(
			com.sun.jersey.client.urlconnection.HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
			new com.sun.jersey.client.urlconnection.HTTPSProperties(
					getHostnameVerifier(), sslContext));



	client = Client.create(defaultClientConfig);


    String url= u.getPropValues("authcronofy");
    System.out.println(url);

	WebResource webResource = client
	   .resource(url);

	//JSONObject jsonObject = new JSONObject();
	 //JSONObject jsonObj = new JSONObject(authdata);

	System.out.println(authdata);
	ClientResponse response = webResource.header("Content-Type", "application/json")
	   .post(ClientResponse.class, authdata.toString());
		System.out.println(response);

	if (response.getStatus() != 200) {
		output = response.getEntity(String.class);


		return Response.status(response.getStatus()).entity(output).build();
	}else
	{
		output = response.getEntity(String.class);
		System.out.println(output);
		JSONObject obj = new JSONObject(output);
		System.out.println("authToken: " + obj.getString("access_token"));
		System.out.println("refreshToken: " + obj.getString("refresh_token"));
		 JSONObject orgdata = new JSONObject();

		 orgdata.put("authToken", obj.getString("access_token"));
		 orgdata.put("refreshToken", obj.getString("refresh_token"));
		 orgdata.put("userId", userid);
		 orgdata.put("accessToGoogleCal",true);
		System.out.println("within success");
		Client client1 = Client.create();

		String url1= u.getPropValues("updatepreferences");
        System.out.println(url1);
		WebResource webResource1 = client1
		   .resource(url1);

		 JSONObject jsonObj1 = new JSONObject();
		 System.out.println(orgdata);
		/*System.out.println(incomingData);*/
		ClientResponse response1 = webResource1.header("Content-Type", "application/json")
		   .post(ClientResponse.class, orgdata.toString());
			System.out.println(response1);
		if (response1.getStatus() != 200) {
			output = response1.getEntity(String.class);
			return Response.status(response1.getStatus()).entity(output).build();
		}
	}




	/*java.net.URI location = new java.net.URI("../");
    return Response.temporaryRedirect(location).build();*/

	

		return Response.status(200).build();
	}



	@POST
	@Path("/pwdReset")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST_password_reset(@Context HttpServletRequest httpServletRequest,String incomingData)throws IOException {
		String output=null;
		 HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);

			Client client = Client.create();

			GetProperties u =new GetProperties();
            String url= u.getPropValues("pwdreset");
            System.out.println(url);
			WebResource webResource = client
			   .resource(url);
			JSONObject jsonObject = new JSONObject();
			 JSONObject jsonObj = new JSONObject(incomingData);

			System.out.println(incomingData);
			ClientResponse response = webResource.header("Content-Type", "application/json")
			   .post(ClientResponse.class, jsonObj.toString());
				System.out.println(response);
			if (response.getStatus() != 200) {
				output = response.getEntity(String.class);
				return Response.status(response.getStatus()).entity(output).build();
			}


			System.out.println("Output from Server .... \n");
			//output = response.getEntity(String.class);
			System.out.println(output);


		// return HTTP response 200 in case of success
		return Response.status(200).entity(output).build();
	}

	
	
	

	@POST
	@Path("/forgotpwd/{login}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response forgot_password(@Context HttpServletRequest httpServletRequest,@PathParam("login") String login)throws IOException {
		String output=null;
		
			Client client = Client.create();

			GetProperties u =new GetProperties();
            String url= u.getPropValues("forgotpwd");
            System.out.println(url);
			WebResource webResource = client
			   .resource(url+login);
			
			ClientResponse response = webResource.header("Content-Type", "application/json")
			   .post(ClientResponse.class);
				System.out.println(response);
			if (response.getStatus() != 200) {
				output = response.getEntity(String.class);
				return Response.status(response.getStatus()).entity(output).build();
			}


			System.out.println("Output from Server .... \n");
			//output = response.getEntity(String.class);
			System.out.println(output);


		// return HTTP response 200 in case of success
		return Response.status(200).entity(output).build();
	}

	
	
	
	
	

	@Path("/upsertSSOUser")
	  @POST
	  @Consumes({MediaType.APPLICATION_JSON})
	  @Produces({MediaType.APPLICATION_JSON})

		   public Response UpsertSSOuser(@Context HttpServletRequest httpServletRequest,String incomingData) throws JSONException,IOException
		 {
		String output=null;


		Client client = Client.create();
		GetProperties u =new GetProperties();
        String url= u.getPropValues("upsertSSOuser");
        System.out.println(url);

		WebResource webResource = client
		   .resource(url);
		JSONObject jsonObject = new JSONObject();
		 JSONObject jsonObj = new JSONObject(incomingData);

		System.out.println(incomingData);
		ClientResponse response = webResource.header("Content-Type", "application/json")
		   .post(ClientResponse.class, jsonObj.toString());
			System.out.println(response);
		if (response.getStatus() != 200) {
			output = response.getEntity(String.class);
			return Response.status(response.getStatus()).entity(output).build();
		}


		System.out.println("Output from Server .... \n");
		output = response.getEntity(String.class);
		System.out.println(output);
		HttpSession Session = httpServletRequest.getSession();
		String Jsessionid = Session.getId();
		System.out.println(Jsessionid);
		NewCookie SessionCookie = new NewCookie("Sessionid", Jsessionid);

	// return HTTP response 200 in case of success
	return Response.status(200).entity(output).cookie(SessionCookie).build();
		 }

	 @Path("/updateUser")
	 @PUT
	 @Consumes({MediaType.APPLICATION_JSON})
	 @Produces(MediaType.APPLICATION_FORM_URLENCODED)

		   public Response UpdateUser(@Context HttpServletRequest httpServletRequest,String incomingData) throws JSONException,IOException
		 {

		 String output=null;
				System.out.print("in updateuser");

				HttpSession Session = httpServletRequest.getSession(false);
				System.out.println(Session);

				if (Session == null) {
					System.out.println("Session is null");

					String result= "Invalid session";
					System.out.println(result);
					return Response.status(403).entity(result).build();
					// it's valid
				}
				String Jsessionid = Session.getId();
				System.out.println(Jsessionid);
				Client client = Client.create();

				GetProperties u =new GetProperties();
	            String url= u.getPropValues("updateuser");
	            System.out.println(url);

	            WebResource webResource = client
				   .resource(url);
				//JSONObject jsonObject = new JSONObject();
				 JSONObject jsonObj = new JSONObject(incomingData);

				System.out.println("-Incoming data : " + incomingData);
				ClientResponse response = webResource.header("Content-Type", "application/json")
				   .put(ClientResponse.class, jsonObj.toString());
					System.out.println(response);
				if (response.getStatus() != 200) {
					output = response.getEntity(String.class);
					return Response.status(response.getStatus()).entity(output).build();
				}

				
				//IM10122016 - If Google Access is Unchecked then call updatePreferences with accesstoGoogle set to false.
				
				JSONObject orgdata = new JSONObject();
				System.out.println("--- " + jsonObj.getBoolean("accessToGoogleCal"));
				if (jsonObj.getBoolean("accessToGoogleCal") == false) {
				
					//orgdata.put("authToken", jsonObj.getString("access_token"));
					//orgdata.put("refreshToken", jsonObj.getString("refresh_token"));
					orgdata.put("userId", jsonObj.getInt("userId"));
					orgdata.put("accessToGoogleCal",jsonObj.getBoolean("accessToGoogleCal"));
					
					Client client1 = Client.create();
	
					String url1= u.getPropValues("updatepreferences");
			        System.out.println(url1);
					WebResource webResource1 = client1.resource(url1);
	
					 //JSONObject jsonObj1 = new JSONObject();
					 System.out.println(orgdata);
					/*System.out.println(incomingData);*/
					ClientResponse response1 = webResource1.header("Content-Type", "application/json")
					   .post(ClientResponse.class, orgdata.toString());
						System.out.println(response1);
					if (response1.getStatus() != 200) {
						output = response1.getEntity(String.class);
						return Response.status(response1.getStatus()).entity(output).build();
					}
	
				}
				 output = response.getEntity(String.class);
				 System.out.println("beforeoutput");
				//System.out.println(output);

				return Response.status(200).entity(output).build();
		 }

	 public void SecureRestClient() {
		}


		private HostnameVerifier getHostnameVerifier() {
			return new HostnameVerifier() {
				public boolean verify(String hostname,
						javax.net.ssl.SSLSession sslSession) {
					return true;
				}

				public boolean verify(String arg0, String arg1) {
					// TODO Auto-generated method stub
					return true;

				}
			};
		}



		@Path("/createEvent")
		  @POST

			   public Response createEvent(@Context HttpServletRequest httpServletRequest,String incomingData) throws JSONException, IOException
			 {

			 String output=null;
					System.out.print("in createEvent");

					HttpSession Session = httpServletRequest.getSession(false);
					System.out.println(Session);

					if (Session == null) {
						System.out.println("Session is null");

						String result= "Invalid session";
						System.out.println(result);
						return Response.status(403).entity(result).build();
						// it's valid
					}
					String Jsessionid = Session.getId();
					System.out.println(Jsessionid);
					Client client = Client.create();

					GetProperties u =new GetProperties();
		            String url= u.getPropValues("event");
		            System.out.println(url);
					WebResource webResource = client
					   .resource(url);
					//JSONObject jsonObject = new JSONObject();
					// JSONObject jsonObj = new JSONObject(incomingData);

					System.out.println(incomingData);
					ClientResponse response = webResource.header("Content-Type", "application/json")
					   .post(ClientResponse.class, incomingData);
						System.out.println(response);
					if (response.getStatus() != 200) {
					//	output = response.getEntity(String.class);
						output = response.getEntity(String.class);
						return Response.status(response.getStatus()).entity(output).build();
					}



					System.out.println(output);

					return Response.status(200).build();
			 }

		@Path("/commChannel")
		  @POST

			   public Response commChannel(@Context HttpServletRequest httpServletRequest,String incomingData) throws JSONException, IOException
			 {

			 String output=null;
					System.out.print("in CommChannel");

					HttpSession Session = httpServletRequest.getSession(false);
					System.out.println(Session);

					if (Session == null) {
						System.out.println("Session is null");

						String result= "Invalid session";
						System.out.println(result);
						return Response.status(403).entity(result).build();
						// it's valid
					}
					String Jsessionid = Session.getId();
					System.out.println(Jsessionid);
					Client client = Client.create();

					GetProperties u =new GetProperties();
		            String url= u.getPropValues("updatechannel");
		            System.out.println(url);
					WebResource webResource = client
					   .resource(url);
					//JSONObject jsonObject = new JSONObject();
					// JSONObject jsonObj = new JSONObject(incomingData);

					System.out.println(incomingData);
					ClientResponse response = webResource.header("Content-Type", "application/json")
					   .post(ClientResponse.class, incomingData);
						System.out.println(response);
					if (response.getStatus() != 200) {
						output = response.getEntity(String.class);
						System.out.println(response.getStatus());
						return Response.status(response.getStatus()).build();
					}



					System.out.println(output);

					return Response.status(200).build();
			 }




		@GET
		@Path("/checklogin/{login}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response checklogin(@Context HttpServletRequest httpServletRequest,@PathParam("login") String login) throws IOException
		{
			/*HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);*/

			Client client = Client.create();
			GetProperties u =new GetProperties();
            String url= u.getPropValues("checklogin");
            System.out.println(url);

			String httpGetUrl=url+login;


			WebResource webResource = client.resource(httpGetUrl);


			ClientResponse response = webResource.get(ClientResponse.class);
			String output=response.getEntity(String.class);

				System.out.println(response);

				if (response.getStatus() != 200) {

						return Response.status(response.getStatus()).entity(output).build();
					}

				System.out.println("Output from Server .... \n");
				//output = response.getEntity(String.class);
				System.out.println(output);


			return Response.status(200).entity(output).build();

		}


		@POST
	        @Path("/updatepreferences")
		public Response updatepermission(@Context HttpServletRequest httpServletRequest,String incomingdata)throws IOException
		{
			String output;
			HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);
			Client client = Client.create();

			GetProperties u =new GetProperties();
            String url= u.getPropValues("updatepreferences");
            System.out.println(url);


		        WebResource webResource = client.resource(url);


				ClientResponse response = webResource.header("Content-Type", "application/json").post(ClientResponse.class, incomingdata);
					System.out.println(response);


				if (response.getStatus() != 200) {
					output = response.getEntity(String.class);
					return Response.status(response.getStatus()).entity(output).build();
				}


			return Response.status(200).build();
		}


	    @GET
            @Produces(MediaType.APPLICATION_JSON)
	    @Path("/getchannels/{userid}")
	    public Response getchannels(@Context HttpServletRequest httpServletRequest,@PathParam("userid") String userid )throws IOException
	    {
	    	String output;
	    	/*HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);*/

	    		Client client = Client.create();
	    		GetProperties u =new GetProperties();
	            String url= u.getPropValues("getchannel");
	            System.out.println(url);

	            url=url+userid;
	                	System.out.println(url);
	    		WebResource webResource = client.resource(url);

	    		ClientResponse response = webResource.header("Content-Type", "application/json").get(ClientResponse.class);
	    			System.out.println(response);


	    		if (response.getStatus() != 200) {
	    			output = response.getEntity(String.class);
					return Response.status(response.getStatus()).entity(output).build();
				}


	    		System.out.println("Output from Server .... \n");
	    		 output = response.getEntity(String.class);
	    		System.out.println(output);


	    	return Response.status(200).entity(output).build();
	    	}




	    @GET
	    @Produces(MediaType.APPLICATION_JSON)
	    @Path("/getreminder/{userid}")
	    public Response getreminderpref(@Context HttpServletRequest httpServletRequest,@PathParam("userid") String userid )throws IOException
	    {

	    	String output;
	    /*	HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);*/

	    		Client client = Client.create();

	    		GetProperties u =new GetProperties();
	            String url= u.getPropValues("getreminder");
	            System.out.println(url);

	             url=url+userid;
	    		    	System.out.println(url);
	    		WebResource webResource = client.resource(url);

	    		ClientResponse response = webResource.header("Content-Type", "application/json").get(ClientResponse.class);
	    			System.out.println(response);


	    		if (response.getStatus() != 200) {
	    			output = response.getEntity(String.class);
					return Response.status(response.getStatus()).entity(output).build();
				}

	    		System.out.println("Output from Server .... \n");
	    		 output = response.getEntity(String.class);
	    		System.out.println(output);

	    	   	return Response.status(200).entity(output).build();

	    	}

	    @POST
	    @Path("/updatechannel")
	    	public Response updatechannel(@Context HttpServletRequest httpServletRequest,String incomingData)throws IOException
	    	{

	    	       HttpSession Session = httpServletRequest.getSession(false);
			       System.out.println(Session);

			        if (Session == null)
			        {
				 System.out.println("Session is null");
				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();

			        }
			      String Jsessionid = Session.getId();
			       System.out.println(Jsessionid);
	        		Client client = Client.create();

	        		GetProperties u =new GetProperties();
	                String url= u.getPropValues("updatechannel");
	                System.out.println(url);

	           		WebResource webResource = client.resource(url);

	      /*  JSONObject jsonObject = new JSONObject();
	        		jsonObject.put("userId", "10");
	        		JSONObject channel = new JSONObject();
	        		channel.put("type","EMAIL");
	        		channel.put("value","Gulnaz");
	        		jsonObject.put("channels", channel);
	        		System.out.println(jsonObject);*/

	        		ClientResponse response = webResource.header("Content-Type", "application/json").post(ClientResponse.class,incomingData);
	        			System.out.println(response);


	        		if (response.getStatus() != 200) {

						return Response.status(response.getStatus()).build();
	        		}


	        	return Response.status(200).build();
	    	}


	    @POST
	    @Path("/updaterempref")
	public Response updaterempref(@Context HttpServletRequest httpServletRequest,String incomingData)throws IOException
	{
		  /*	HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

			if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
				return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);*/

	    		Client client = Client.create();

	    		GetProperties u =new GetProperties();
	            String url= u.getPropValues("updatereminder");
	            System.out.println(url);

	    		WebResource webResource = client.resource(url);

	    		//String rempref="{\"userId\":\"10\",\"reminderPreferences\":[{\"channelType\":\"EMAIL\",\"reminderNumber\":\"10\",\"reminderType\":\"MINUTES\"}]}";
	        //  JSONObject jsonObject = new JSONObject(rempref);
            //System.out.println(jsonObject);

	    		ClientResponse response = webResource.header("Content-Type", "application/json").post(ClientResponse.class,incomingData);
	    			System.out.println(response);


	    		if (response.getStatus() != 200) {
	    			return Response.status(response.getStatus()).build();
	    		}


	 	    	return Response.status(200).build();
	}


	    @PUT
        @Path("/updateevent")
        public Response updateEvent(@Context HttpServletRequest httpServletRequest,String incomingData) throws JSONException,IOException
	 {

             HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

	         if (Session == null)
                     {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
			return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);

                   	Client client = Client.create();
                	GetProperties u =new GetProperties();
                    String url= u.getPropValues("event");
                    System.out.println(url);

	WebResource webResource = client.resource(url);
			ClientResponse response = webResource.header("Content-Type", "application/json").put(ClientResponse.class, incomingData);
				System.out.println(response);
			if (response.getStatus() != 200) {

				System.out.println(response.getStatus());
				return Response.status(response.getStatus()).build();
			}

			return Response.status(200).build();
	 }


    @DELETE
    @Path("/deleteevent")
    public Response deleteEvent(@Context HttpServletRequest httpServletRequest,@QueryParam("eventid") String eventid) throws JSONException, IOException
	 {

            HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);

	          if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
			return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);

            	Client client = Client.create();
            	GetProperties u =new GetProperties();
                String url= u.getPropValues("event");
                System.out.println(url);

			WebResource webResource = client
			   .resource(url+"/"+eventid);

				ClientResponse response = webResource.header("Content-Type", "application/json").delete(ClientResponse.class);
				System.out.println(response);
			if (response.getStatus() != 200) {

				System.out.println(response.getStatus());
				return Response.status(response.getStatus()).build();
			}

			return Response.status(200).build();
	 }


      @GET
      @Path("/getevent")
      @Produces({MediaType.APPLICATION_JSON})
      
      public Response getEvent(@Context HttpServletRequest httpServletRequest,@QueryParam("startdate") String startdate,@QueryParam("enddate") String enddate,@QueryParam("userid") String userid,@QueryParam("timeOffSet") String timeOffSet) throws JSONException,IOException
	 {
		String output=null;
                 HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);
			System.out.println(timeOffSet);
			timeOffSet=URLEncoder.encode(timeOffSet);

	           if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
			return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);

           	   Client client = Client.create();
           	GetProperties u =new GetProperties();
            String url= u.getPropValues("event");
            System.out.println(url);


		WebResource webResource = client.resource(url+"/"+userid+"?startDate="+startdate+"&endDate="+enddate+"&timeOffSet="+timeOffSet);

			ClientResponse response = webResource.header("Content-Type", "application/json").get(ClientResponse.class);
				System.out.println(response);
			if (response.getStatus() != 200) {

				System.out.println(response.getStatus());
				return Response.status(response.getStatus()).build();
			}
			output = response.getEntity(String.class);
		//	System.out.println(response.getEntity(String.class));

                    return Response.status(200).entity(output).build();
	 }

      @GET
      @Path("/geteventdetails")
      @Produces({MediaType.APPLICATION_JSON})
      
      public Response getEventdetails(@Context HttpServletRequest httpServletRequest,@QueryParam("eventid") String eventid,@QueryParam("timeOffSet") String timeOffSet) throws JSONException,IOException
	 {
		String output=null;
                 HttpSession Session = httpServletRequest.getSession(false);
			System.out.println(Session);
			System.out.println(timeOffSet);
			timeOffSet=URLEncoder.encode(timeOffSet);

	           if (Session == null) {
				System.out.println("Session is null");

				String result= "Invalid session";
				System.out.println(result);
			return Response.status(403).entity(result).build();
				// it's valid
			}
			String Jsessionid = Session.getId();
			System.out.println(Jsessionid);

           	   Client client = Client.create();
           	GetProperties u =new GetProperties();
            String url= u.getPropValues("geteventdetails");
            System.out.println(url);


		WebResource webResource = client.resource(url+"/"+eventid+"?timeOffSet="+timeOffSet);

			ClientResponse response = webResource.header("Content-Type", "application/json").get(ClientResponse.class);
				System.out.println(response);
			if (response.getStatus() != 200) {

				System.out.println(response.getStatus());
				return Response.status(response.getStatus()).build();
			}
			output = response.getEntity(String.class);
		//	System.out.println(response.getEntity(String.class));

                    return Response.status(200).entity(output).build();
	 }
      
      
      @Path("/logout")
    	@POST
    	@Consumes({ MediaType.APPLICATION_JSON })
    	@Produces({ MediaType.APPLICATION_JSON })
    	public Response logout(@Context HttpServletRequest httpServletRequest)
    			throws Exception {

    		// JSONObject jsonObject1=new JSONObject(myjson);

    		JSONObject jsonObject = new JSONObject();

    		HttpSession Session = httpServletRequest.getSession(false);
    		System.out.println(Session);
    		if (Session == null) {
    			System.out.println("Session is null");
    			jsonObject.put("status", "FAILURE");
    			jsonObject.put("Code", "2");
    			String result = jsonObject.toString();

    			System.out.println(result);
    			return Response.status(200).entity(result).build();
    			// it's valid
    		}
    		else{
    			String Jsessionid = Session.getId();
    			Session.invalidate();
    			System.out.println(Jsessionid);
    			NewCookie SessionCookie = new NewCookie("Sessionid", Jsessionid);
    			String result = jsonObject.toString();

    			System.out.println(result);
    			return Response.status(200).entity(result).cookie(SessionCookie)
    					.build();
    		}

    		

    	}

  /*	@GET
  	@Path("/webhook")
  	@Produces(MediaType.TEXT_PLAIN)
  	public Response fbwebhook(@Context HttpServletRequest httpServletRequest,
  		@QueryParam("hub.verify_token") String verify_token,
  		@QueryParam("hub.mode") String mode,
  		@QueryParam("hub.challenge") String challenge) 
  			throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException,IOException{

  		//JSONObject resp = new JSONObject();
  		//resp.put("hub.challenge", challenge);
  		GetProperties u =new GetProperties();
  		String mode_value= u.getPropValues("mode_value");
  		String verification_code= u.getPropValues("verification_code");
  		//System.out.println(mode);
  		//System.out.println(verify_token);
  		if (mode.equals(mode_value) && verify_token.equals(verification_code)) {
  			//System.out.println("validating");
  			//String result = resp.toString();
  			//System.out.println(result);
  			return Response.status(200).entity(challenge).build();
  		} else {
  			//System.out.println("Failed validation. Make sure the validation tokens match.");
  			return Response.status(403).build();
  		}
  	}*/
      
      
      
  	@GET
  	@Path("/webhook")
  	@Produces(MediaType.TEXT_PLAIN)
  	public Response fbwebhook(@Context HttpServletRequest httpServletRequest,
  		@QueryParam("hub.verify_token") String verify_token,
  		@QueryParam("hub.mode") String mode,
  		@QueryParam("hub.challenge") String challenge) 
  			throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException,IOException{

  		//JSONObject resp = new JSONObject();
  		//resp.put("hub.challenge", challenge);
  		GetProperties u =new GetProperties();
  		String mode_value= u.getPropValues("mode_value");
  		String verification_code= u.getPropValues("verification_code");
  		//System.out.println(mode);
  		//System.out.println(verify_token);
  		if (mode.equals(mode_value) && verify_token.equals(verification_code)) {
  			//System.out.println("validating");
  			//String result = resp.toString();
  			//System.out.println(result);
  			return Response.status(200).entity(challenge).build();
  		} else {
  			//System.out.println("Failed validation. Make sure the validation tokens match.");
  			return Response.status(403).build();
  		}
  	}
  	
  	@POST
  	@Path("/webhook")
  	@Produces(MediaType.TEXT_PLAIN)
  	public Response fbwebhookpost(@Context HttpServletRequest httpServletRequest) 
		throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, IOException{
	
		return Response.status(200).build();
  	}
  	
  	
}